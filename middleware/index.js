const express = require('express');
const { Pool } = require('pg');
const app = express();
const server = require('http').Server(app);
const jwt = require('jsonwebtoken');
const axios = require('axios');
const bodyParser = require('body-parser');
const port = process.env.PORT || 9000;
global.io = require('socket.io')(server);
const fs = require('fs');
global.token = null;
global.kiosk = [];
global.worklistGroup = [];
global.endkiosk = [];
global.statusscan = '';
// global.kiosklocate = [];
// app.set('socket', io);
// const authRoute = require('./routes/auth');

const patientRoute = require('./routes/patient');
const smartRoute = require('./routes/smart');
const basicAuth = require('./helpers/basic-auth');
const errorHandler = require('./helpers/error-handler');
const patientTypePayor = require('./routes/patient-type-payor');
const patientAppointment = require('./routes/appointment');
const patientWorklist = require('./routes/worklist');
const patientOpenConverage = require('./routes/openconverage');
const patientGenQueue = require('./routes/genQueue');
const patientPrintQueue = require('./routes/printQueue');
const patientRePrintQueue = require('./routes/reprint');
const userManagement = require('./routes/management');
const testPatientJson = require('./routes/patientjson')(io);
const findIndex = require('./function/findIndex');
const openCredit = require('./routes/postCredit');


// Get Mock JSON
const getPatientOldHIS = require('./api/getOldPatient');
const getAppointmentHIS = require('./api/getAppointment');
const postOpenCredit = require('./api/postOpenCredit');
const mockCredit = require('./api/mockupCredit'); //Overwrite
// END Get Mock JSON

process.on('uncaughtException', function (error) {
    // console.error(error);
});

io.on('connection', (client) => {
   
    // //console.logclient.id);

    // client.emit('response_test_data', 'Test');
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "*"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, DELETE, OPTIONS"
    );
    // res.setHeader('Content-Type', 'text/plain');
    // res.setHeader('Content-Type', 'multipart/form-data');
    // res.setHeader('Content-Type', 'application/json');
    next();
});

app.use(basicAuth);

// api routes
app.use('/users', require('./users/users.controller'));
// global error handler
app.use(errorHandler);
// check authen jwt token
// app.use('/api/auth', authRoute);


// Smart Card
app.use('/api/smart', smartRoute);
// kiosk patient check HN or No HN
app.use('/api/kiosk', patientRoute);
// Kiosk Patient Type & Payor
app.use('/api/kiosk/tp', patientTypePayor);
// Kiosk Check Appointment
app.use('/api/kiosk/ap', patientAppointment);
// Kiosk Worklist Processing
app.use('/api/kiosk/worklist', patientWorklist);
// Kiosk Open Converage (PAYOR)
app.use('/api/kiosk/oc', patientOpenConverage);
// Kiosk Gen Queue Number
app.use('/api/kiosk/gen', patientGenQueue);
// Kiosk Print & Show Queue Worklist Process
app.use('/api/kiosk/', patientPrintQueue);
// Kiosk Print & Show Queue Worklist Process
app.use('/api/kiosk/', patientRePrintQueue);
// Kiosk Open Credit
app.use('/api/kiosk/creditmaster', openCredit);

// Management
app.use('/api/manage', userManagement);




// GET MOCK DATA JSON

app.use('/ords/pmkords/hlab', getPatientOldHIS);
app.use('/ords/pmkords/hlab', getAppointmentHIS);
app.use('/ords/pmkords/hlab', postOpenCredit);
app.use('/ords/pmkords/hlab', mockCredit);

// END GET MOCK DATA JSON


server.listen(port, function () {
    // getData(); // On load function getData() JWT Authen
    // console.log('Server is running.. on Port %d', port);
});

// const pool = new Pool({
//     user: 'queueregis',
//     host: '191.123.58.47',
//     database: 'pmk_queue',
//     password: 'P@ssw0rd1168',
//     port: 5432
// });


const pool2 = new Pool({
    user: 'queueregis',
    host: '191.123.58.47',
    database: 'pmk_authen',
    password: 'P@ssw0rd1168',
    port: 5432
});

pool2.connect(function(err) {
    pool2.query("SELECT token FROM authen_user WHERE username='peecz'", function (err, results) {
        // //console.logresults)
          if (!err){
              //Return the fields object:
              if(results){
                  global.token = results.rows[0].token;
              }
              return global.token;
          }else{
              //console.log'ERROR');
          }
    });

});

// pool.connect(function(error){
//     if(!!error){
//       //console.log'Error');
//     }else{
//       //console.log'Connected !');
//     }
// });





// // ------------------------ Test Socket
app.get('/testemit', (req, res) => {
    var data = [];
    if(data != null){
        data = 'Success';
        io.emit('testSocket', data);
        res.status(200).json(data);
    }else{
        io.emit('testSocket', data);
        res.status(403).json({success:false});
    }
});

// app.post('/testpostemit', (req, res) => {x
//     // //console.logreq.body, 'Test Post Response');
//     var data = [];
//     if(req.body){
//         data = req.body;
//         //console.logdata, 'Test Data Post');
//         io.emit('testSocket', data);
//         res.status(200).json(data);
//     }else{
//         io.emit('testSocket', data);
//         res.status(403).json({success:false});
//     }
// });
// ------------------------ End Test Socket

