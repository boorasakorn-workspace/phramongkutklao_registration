const router = require('express').Router();
const { Pool } = require('pg');
const findIndex = require('../function/findIndex');
const config = require('./../function/connect');

const axios = require('axios');
const pool = new Pool(config);


router.post('/queueno', (req, res) => {
    var data = req.body;
    var idworklistgroup = data.worklistgroup;
    // console.log(data, "TEST WLG");
    var idpayor = data.payor;
    var typecode = data.persontype;
    var cardid = '';
    var prename = '';
    var forename = '';
    var surname = '';
    var dob = '';
    var infoData = '';
    var hnno = '';
    var dataInfo = '';
    var clinicname = '';
    
    if(req.body.userinfo){
        dataInfo = JSON.parse(req.body.userinfo);
        
        if (dataInfo.data1.hnno == '' || dataInfo.data1.hnno == 'undefined' || dataInfo.data1.hnno == null) {
            hnno = '';
        } else {
            hnno = dataInfo.data1.hnno;
        }

        infoData = { cardid: chkVal(dataInfo.data1.idcard), prename: chkVal(dataInfo.data1.prefixname_th), forename: chkVal(dataInfo.data1.firstname_th), surname: chkVal(dataInfo.data1.lastname_th), dob: chkVal(dataInfo.data1.issuedate) };
        // console.log(infoData, "ISSUEDATE")
        if (data.clinicname != '') {
            clinicname = data.clinicname;
        }
    }else{
        infoData = { cardid: '', prename: '', forename: '', surname: '', dob: '' };

    }


    //console.log(infoData, 'INFO DATA2')
    //console.log(data, 'GEN QUEUEEEE')

    //console.logtypeof dataInfo.data1.hnno, "CHK DATA TYPE HN")

    //console.loginfoData, 'FALSE')

    function chkVal(values) {
        if (values == '' || typeof values == 'undefined') {
            return '';
        } else {
            return values;
        }
    }

    GET_TYPEUID(typecode, (res_typeuid) => {
        //console.log(res_typeuid)
        if (res_typeuid.success) {
            CHK_PATIENTINFO(infoData.cardid, (res_chkpatient) => {
                if (res_chkpatient.success) {
                    //console.log(res_chkpatient, '2 INSERT')
                    if(res_chkpatient.data != ''){
                        INSERT_PATIENTINFO({ infoData: infoData, issuedate: dataInfo.data1.issuedate, idworklistgroup: idworklistgroup, hnno: hnno, res_typeuid: res_typeuid, payoruid: idpayor, clinicname: clinicname }, (res_patientinfo) => {
                            if (res_patientinfo.success) {
                            //console.log(res_patientinfo, "RESULT PATIENT")
                            GET_WORKLISTGROUPPROCESS({ patientuid: res_patientinfo.data.uid, worklistgroup: idworklistgroup }, (res_getworklistgroupprocess) => {
                                //console.log(res_getworklistgroupprocess, "RESULT GET WORKLIST GROUP PROCESS")
                                if (res_getworklistgroupprocess.success) {
                                    GET_GROUPPROCESS({ typeuid: res_typeuid.data.uid, payoruid: idpayor, patientuid: res_patientinfo.data.uid, groupprocessuid: res_getworklistgroupprocess.data }, (res_groupprocess) => {

                                        //console.log(res_groupprocess, "RESULT res_groupprocess 1")
                                        if (res_groupprocess.success) {
                                            //console.log(res_groupprocess, "RESULT res_groupprocess 2")
                                            var sql_values = "";
                                            for (var i = 0; i < res_groupprocess.data.length; i++) {
                                                sql_values = sql_values + ",('" + res_groupprocess.patientuid + "',(select CONCAT('" + res_groupprocess.data[i].code + "',lpad((coalesce(count(uid::integer),0)+1)::text, 3, '0')) from tr_patientqueue where cwhen::date = now()::date and queuecategoryuid = '" + res_groupprocess.data[i].queuecategoryuid + "'),now(),'" + res_groupprocess.data[i].queuecategoryuid + "','" + res_groupprocess.data[i].groupprocessuid + "',CONCAT((select CONCAT('" + res_groupprocess.data[i].code + "',lpad((coalesce(count(uid::integer),0)+1)::text, 3, '0')) from tr_patientqueue where cwhen::date = now()::date and queuecategoryuid = '" + res_groupprocess.data[i].queuecategoryuid + "'),now()::date)) ";
                                            }
                                            if (sql_values != "") {
                                                var new_values = sql_values.substr(1);
                                                INSERTUPDATE_GENQUEUE({ res_groupprocess: res_groupprocess, new_values: new_values, typeuid: res_typeuid.data.uid, refno: res_patientinfo.data.refno, kiosk_location: req.body.kiosk_location });
                                            }


                                        } else {
                                            //console.log"TEST 2")
                                            var data = [];
                                            var data1 = { Patientuid: res_patientinfo.data.uid, queueno: '', queuecategoryuid: '', groupprocessuid: '', refno: res_patientinfo.data.refno, kiosk_location: req.body.kiosk_location };
                                            data.push({ 'data': data1 });
                                            //console.logdata, "TEST DATA NO GEN QUEUE")
                                            global.io.emit('genqueue', data);
                                        }
                                    });
                                } else {
                                    var data = [];
                                    var data1 = { Patientuid: res_patientinfo.data.uid, queueno: '', queuecategoryuid: '', groupprocessuid: '', refno: res_patientinfo.data.refno, kiosk_location: req.body.kiosk_location };
                                    data.push({ 'data': data1 });
                                    //console.logdata, "TEST DATA NO GEN QUEUE")
                                    global.io.emit('genqueue', data);
                                }

                            });

                        }
                        });
                    }else{
                        INSERT_PATIENTINFO({ infoData: infoData, idworklistgroup: idworklistgroup, hnno: hnno, res_typeuid: res_typeuid, payoruid: idpayor, clinicname: clinicname }, (res_patientinfo) => {
                            if (res_patientinfo.success) {
                                //console.log(res_patientinfo, "RESULT PATIENT")
                                GET_WORKLISTGROUPPROCESS({ patientuid: res_patientinfo.data.uid, worklistgroup: idworklistgroup }, (res_getworklistgroupprocess) => {
                                    //console.log(res_getworklistgroupprocess, "RESULT GET WORKLIST GROUP PROCESS")
                                    if (res_getworklistgroupprocess.success) {
                                        GET_GROUPPROCESS({ typeuid: res_typeuid.data.uid, payoruid: idpayor, patientuid: res_patientinfo.data.uid, groupprocessuid: res_getworklistgroupprocess.data }, (res_groupprocess) => {
    
                                            //console.log(res_groupprocess, "RESULT res_groupprocess 1")
                                            if (res_groupprocess.success) {
                                                //console.log(res_groupprocess, "RESULT res_groupprocess 2")
                                                var sql_values = "";
                                                for (var i = 0; i < res_groupprocess.data.length; i++) {
                                                    sql_values = sql_values + ",('" + res_groupprocess.patientuid + "',(select CONCAT('" + res_groupprocess.data[i].code + "',lpad((coalesce(count(uid::integer),0)+1)::text, 3, '0')) from tr_patientqueue where cwhen::date = now()::date and queuecategoryuid = '" + res_groupprocess.data[i].queuecategoryuid + "'),now(),'" + res_groupprocess.data[i].queuecategoryuid + "','" + res_groupprocess.data[i].groupprocessuid + "',CONCAT((select CONCAT('" + res_groupprocess.data[i].code + "',lpad((coalesce(count(uid::integer),0)+1)::text, 3, '0')) from tr_patientqueue where cwhen::date = now()::date and queuecategoryuid = '" + res_groupprocess.data[i].queuecategoryuid + "'),now()::date)) ";
                                                }
                                                if (sql_values != "") {
                                                    var new_values = sql_values.substr(1);
                                                    INSERTUPDATE_GENQUEUE({ res_groupprocess: res_groupprocess, new_values: new_values, typeuid: res_typeuid.data.uid, refno: res_patientinfo.data.refno, kiosk_location: req.body.kiosk_location });
                                                }
    
    
                                            } else {
                                                //console.log"TEST 2")
                                                var data = [];
                                                var data1 = { Patientuid: res_patientinfo.data.uid, queueno: '', queuecategoryuid: '', groupprocessuid: '', refno: res_patientinfo.data.refno, kiosk_location: req.body.kiosk_location };
                                                data.push({ 'data': data1 });
                                                //console.logdata, "TEST DATA NO GEN QUEUE")
                                                global.io.emit('genqueue', data);
                                            }
                                        });
                                    } else {
                                        var data = [];
                                        var data1 = { Patientuid: res_patientinfo.data.uid, queueno: '', queuecategoryuid: '', groupprocessuid: '', refno: res_patientinfo.data.refno, kiosk_location: req.body.kiosk_location };
                                        data.push({ 'data': data1 });
                                        //console.logdata, "TEST DATA NO GEN QUEUE")
                                        global.io.emit('genqueue', data);
                                    }
    
                                });
    
                            }
                        });
                    }
                        
                }
            });

        } else {
            //console.log'FALSE')
        }

    })


    //console.log'genQueue');



});

var CHK_PATIENTINFO = (idcard, callback) => {
    //console.logidcard, "IDDDD")
    var sql = "SELECT * FROM tr_patient WHERE idcard = '" + idcard + "' AND active='Y' ";
    if(idcard == ''){
        callback({ success: true, data: '' })
    }else{
        pool.query(sql, (err, results) => {
            //console.log(results.rows, "FFFFFFFFFFFFF")
            if (!err) {
                if (results.rowCount > 0) {
                    // //console.logresults.rows[0], 'CHKPATIENT')
                    callback({ success: true, data: results.rows[0] })
                } else {
                    // //console.logresults.rows[0], 'NONE')
                    callback({ success: true, data: '' })
                }
            } else {
                callback({ success: false })
            }

        });
    }
}

var INSERT_PATIENTINFO = (infoData, callback) => {
    console.log(infoData, 'TESTBBBB')
    var ref = "r" + Math.floor(100000 + Math.random() * 900000);
    //    //console.logchkRefnoOnPatient(), "TC BBBB");
    var sql = "INSERT INTO tr_patient (refno,hn,idcard,dob,payorid,patienttypeid,prename,forename,surname,active,cwhen,worklistgroupuid,clinicname) SELECT '" + ref + "' as refno, '" + infoData.hnno + "' as hn, '" + infoData.infoData.cardid + "' as idcard,'" + infoData.infoData.dob + "' as dob,'" + infoData.payoruid + "' as payorid,'" + infoData.res_typeuid.data.uid + "' as patienttypeid,'" + infoData.infoData.prename + "' as prename,'" + infoData.infoData.forename + "' as forename,'" + infoData.infoData.surname + "' as surname,'Y',now() as active,'" + infoData.idworklistgroup + "' as worklistgroupuid, '" + infoData.clinicname + "' RETURNING uid,refno";
    //console.log(sql, "TEST iNSERT")
    pool.query(sql, (err, results) => {
        if (!err) {
            // if (results.rowCount > 0) {
            //console.log(sql, "TESTCCCCCC")
            callback({ success: true, data: results.rows[0] })
            // } else {
            //     //console.logsql, "TESTCCCCCC")
            //     callback({ success: true })
            // }
        } else {
            //console.log(err, "TESTDD")
            // callback({ success: false })
        }

    });
}

var UPDATE_PATIENTINFO = (infoData, callback) => {
    //console.loginfoData, 'TESTAAAA')
    var sql = "UPDATE tr_patient SET payorid='" + infoData.payoruid + "', patienttypeid='" + infoData.res_typeuid.data.uid + "', active='Y',mwhen=now() WHERE idcard='" + infoData.infoData.cardid + "'  RETURNING uid";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                callback({ success: true, data: results.rows[0] })
            } else {
                callback({ success: false })
            }
        } else {
            callback({ success: false })
        }

    });
}

var GET_TYPEUID = (typecode, callback) => {
    var sql = "SELECT uid FROM tb_patienttype WHERE code='" + typecode + "'";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                callback({ success: true, data: results.rows[0] })
            } else {
                callback({ success: false })
            }

        } else {
            callback({ success: false })
        }
    });
}
var GET_GROUPPROCESS = (data, callback) => {
    // //console.logdata.groupprocessuid, "TEST RESULT GROUPPROCESS 55555")
    var groupprocessuid = [];
    for (var k = 0; k < data.groupprocessuid.length; k++) {
        groupprocessuid.push(data.groupprocessuid[k].groupprocessuid)
    }

    //console.loggroupprocessuid, "TEST RESULT GROUPPROCESS 55555")

    var queryQueueCategory = "SELECT * FROM vw_queuecategory WHERE groupprocessuid in (" + groupprocessuid + ") and patienttypeuid = '" + data.typeuid + "' and payoruid = '" + data.payoruid + "'";
    pool.query(queryQueueCategory, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                //console.logresults.rows, "TEST RESULT GROUPPROCESS")
                callback({ success: true, data: results.rows, patientuid: data.patientuid })
            } else {
                callback({ success: false })
            }
        } else {
            callback({ success: false })
        }

    });
}

var INSERTUPDATE_GENQUEUE = (param) => {
    var sql = "insert into tr_patientqueue (patientuid,queueno,cwhen,queuecategoryuid,groupprocessuid,queueunique) values " + param.new_values + " returning uid,queueno,queuecategoryuid,groupprocessuid,patientuid";
    pool.query(sql, (err, results) => {
        //console.logerr, "TEST ERROR GENQUEUE")
        if (!err) {
            if (results.rowCount > 0) {
                var sql_update_values = "";
                for (var i = 0; i < results.rows.length; i++) {
                    sql_update_values = sql_update_values + ",('" + param.res_groupprocess.data[i].queuecategoryuid + "',now(),'" + results.rows[i].queueno + "','" + param.res_groupprocess.data[i].groupprocessuid + "')";
                }
                if (sql_update_values != "") {
                    var new_update_values = sql_update_values.substr(1);
                    var sql = "INSERT INTO tr_queuenumber (queuecategoryuid, createdate, lastqueue,groupprocessuid) VALUES " + new_update_values + " ON CONFLICT (queuecategoryuid) DO UPDATE SET lastqueue = excluded.lastqueue, mwhen = now()";
                    //console.logsql, "INSERT DUPLICATE");
                    pool.query(sql, (err, results) => {
                        if (!err) {
                            //console.log'UPDATE TR_QUEUENUMBER')
                        }
                    });
                    var data = { data: results.rows, typeuid: param.typeuid, refno: param.refno, kiosk_location: param.kiosk_location };
                    //console.logdata, 'TRPATIENTQUEUE');
                    global.io.emit('genqueue', data);
                }

            } else {
                //console.logerr)
            }

        } else {
            if (err.code == 23505) {
                INSERTUPDATE_GENQUEUE(param);
            }
            //console.logerr)
        }
    });
}

var GET_WORKLISTGROUPPROCESS = (data, callback) => {
    var sql = "SELECT groupprocessuid FROM vw_worklistprocess WHERE worklistgroup='" + data.worklistgroup + "' AND groupprocessuid is not null"

    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                //console.logresults.rows[0], 'TEST SQL')
                callback({ success: true, data: results.rows })
            } else {
                callback({ success: false })
            }

        } else {
            callback({ success: false })
        }
    });
}

// function chkRefnoOnPatient() {
//     var refno = 0;

//     var ref = "r"+Math.floor(100000 + Math.random() * 900000);
//         var sql = "SELECT refno FROM tr_patient WHERE refno = '"+ref+"'";
//         pool.query(sql, (err, res) => {
//             done();
//             if(!err){
//                 if(res.rowCount <1){
//                     refno = ref
//                     return ref;
//                 }
//             }
//         });
// //   refno;
// }


module.exports = router;