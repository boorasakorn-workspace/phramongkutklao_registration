const router = require('express').Router();
const { Pool } = require('pg');
const findIndex = require('./../function/findIndex');
const config = require('./../function/connect');
// const config1 = require('./../function/connectapi');
var dateFormat = require('dateformat');

const axios = require('axios');
const pool = new Pool(config);
// const pool1 = new Pool(config1);
// ////console.logpool, 'pool');
// ////console.logpool1, 'pool1');


////console.logdatetime, 'date now appointment')
// ------------------------ API Smart Card From Scan
// Detail
router.post('/smartcard/info', (req, res) => {

    //Overwrite Emitter
    if (req.body) {
        let scanner_response = {
            card : 'scanner',
            kiosk_location : req.body.kiosk_location,
            data : req.body
        };
        global.io.emit('kiosk_hn', scanner_response);
    }


    var d = new Date();
    var now = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
    var datetime = dateFormat(now, "yyyy-mm-dd");
    
    // console.log(req.body, "TEST SCAN DATA KIOSK LOCATION");
    var infop = [];
    var data = req.body;
    var card = req.body.citizenid;
    var hnnull = null;
    var hnnull2 = null;
    req.body.idcard = req.body.citizenid;
    req.body = { 0: req.body };
    //console.log(req.body, 'TEST SCAN DATA KIOSK LOCATION')
    if (req.body) {
        clearEndkioskLocation(req.body[0].kiosk_location);
        clearKioskLocation(req.body[0].kiosk_location);
        chkApiOldPatient({ hn: hnnull + '/' + hnnull2, idcard: card }, (res_chkoldpatient) => {
            if(res_chkoldpatient.error){
                res.status(200).send({ success: false, error:500, api_his:false, api_appointment: true, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
            }else{
                var hn = res_chkoldpatient.data.hn;
                var idcard = res_chkoldpatient.data.pid;
                var preName = res_chkoldpatient.data.preName;
                if (preName == '' || preName == undefined || preName == null) {
                    preName = 'คุณ';
                }
                var firstName = res_chkoldpatient.data.firstName;
                var lastName = res_chkoldpatient.data.lastName;
                var birthDate = res_chkoldpatient.data.birthDate;
                var gender = '';
                var genderscan = '';
                console.log(res_chkoldpatient.data, 'TEST GENDER 1')
                if(res_chkoldpatient.data.gender){
                    if(res_chkoldpatient.data.gender != '' || res_chkoldpatient.data.gender != null || typeof(res_chkoldpatient.data.gender != undefined)){
                        gender = res_chkoldpatient.data.gender;
                        console.log(gender, 'TEST GENDER 2')
                    }
                }else{
                    genderscan = req.body[0].gender;
                    console.log(genderscan, 'TEST GENDER 3')
                    if(genderscan == 0){
                        gender = '';
                    }else if(genderscan == 1){
                        gender = 'male';
                    }else if (genderscan == 2){
                        gender = 'female';
                    }

                }
                var payorid = '';
                var type = '';
                if (res_chkoldpatient.success && res_chkoldpatient.data != '') {
                    //console.log(res_chkoldpatient, "TEST API CHK OLD PATIENT SCAN");
                    var birth = res_chkoldpatient.data.birthDate;
                    var bd = '';
                    var birthDate = '';
                    var fullbirth = '';
                    if (birth != null) {
                        if (birth.includes('-')) {
                            bd = birth.split('-');
                        }
                        //console.log(bd, 'birth dateee');
                        fullbirth = (parseInt(bd[0]) + 543) + '-' + bd[1] + '-' + bd[2];
                        birthDate = dateFormat(fullbirth, `dd/mm/yyyy`);
                    }

                    chkIdcardFromDB({ hn: hn, idcard: card, firstName: firstName, lastName: lastName, birthDate: birthDate }, (res_chkidcard) => {
                        ////console.logres_chkidcard.data, "TEST CHK PATIENT FROM ID CARD SCAN")
                        if (res_chkidcard.success) {
                            if (res_chkidcard.data.active == 'Y') {
                                var group = '1';
                                getPatientRePrint({ uid: res_chkidcard.data.uid, group: res_chkidcard.data.worklistgroupuid }, (res_patientinfo) => {
                                    //console.logres_patientinfo, "TEST REPRINT")
                                    if (res_patientinfo.success) {
                                        var datainfo = '';
                                        for (var i = 0; i < res_patientinfo.data.length; i++) {
                                            var flownameRoom = '';
                                            // dataResult = res_patientinfo.data[i];
                                            var vscard1 = res_patientinfo.data[i].idcard.substr(0, 4, res_patientinfo.data[i].idcard);
                                            var vscard2 = res_patientinfo.data[i].idcard.substr(4, 5, res_patientinfo.data[i].idcard);
                                            var vscard3 = res_patientinfo.data[i].idcard.substr(9, 4, res_patientinfo.data[i].idcard);

                                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                            var citizenid_xxx = vscard1 + vscard4 + vscard3;
                                            datainfo = { hnno: hn, gender: gender, patient_uid: res_patientinfo.data[i].patientuid, idcard: res_patientinfo.data[i].idcard, citizenid_xxx: citizenid_xxx, prefixname_th: req.body[0].prefixname_th, firstname_th: res_patientinfo.data[i].forename, lastname_th: res_patientinfo.data[i].surname, issuedate: res_patientinfo.data[i].dob, patient_uid: res_patientinfo.data[i].patientuid };

                                            if (res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != '') {
                                                flownameRoom = ' ' + res_patientinfo.data[i].clinicname.replace(".", ",");
                                            }
                                            ////console.logres_patientinfo.data[i].patientuid, "TEST REPRINT 2")
                                            var dddd = {
                                                "location": '1',
                                                "sequence": i + 1,
                                                "title": res_patientinfo.data[i].title,
                                                "flowname": res_patientinfo.data[i].flowname + flownameRoom,
                                                "waiting": "",
                                                "queueno": res_patientinfo.data[i].queueno,
                                                "worklistuid": res_patientinfo.data[i].worklistuid,
                                                "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                                                "infoData": datainfo,
                                                "clinicname": res_patientinfo.data[i].clinicname,
                                                "group": res_patientinfo.data[i].worklistgroup,
                                                "closeselectone": res_patientinfo.data[i].closeselectone,
                                                "closevs": res_patientinfo.data[i].closevs,
                                                "closenewhn": res_patientinfo.data[i].closenewhn,
                                                "closepayor": res_patientinfo.data[i].closepayor,
                                                "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                                                "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                                                "cancelregister": res_patientinfo.data[i].cancelregister,
                                                "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                                                "kiosk_location": req.body[0].kiosk_location
                                            }
                                            global.endkiosk.push(dddd);
                                            if (i == res_patientinfo.data.length - 1) {
                                                res.status(200).send({ success: true, data: global.endkiosk, step: 'next8', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location });
                                                global.io.emit('kiosk_hn', { data: global.endkiosk, step: "next8scan", kiosk_location: req.body[0].kiosk_location });
                                            }
                                        }


                                    }

                                });
                            } else {
                                var dataResult = [];
                                var fullHn = '';
                                var s = hn.split('/');
                                fullHn = s[1] + '/' + s[0];
                                //console.log(fullHn, "TEST HN SMART 1")
                                getAppointmentDataAPI({ hnnumber: fullHn, dateappointment: datetime }, (res_getappointment) => {
                                    if(res_getappointment.error == 500 || res_getappointment.error == 404){
                                        // res.status(200).send({ success: false, error:500, api_his:true, api_appointment:false, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                        var group = '1';

                                        var vscard1 = idcard.substr(0, 4, idcard);
                                        var vscard2 = idcard.substr(4, 5, idcard);
                                        var vscard3 = idcard.substr(9, 4, idcard);
    
                                        var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                        var citizenid_xxx = vscard1 + vscard4 + vscard3;
                                        
                                        var datainfo = { hnno: hn, gender: gender, patient_uid: res_chkidcard.data.uid, idcard: card, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: res_chkidcard.payorid, type: res_chkidcard.patienttypeid, payorname: '', payorcode: '' };
                                        dataResult.push(datainfo);
                                        var data = {success: false, error:500, api_his:true, api_appointment:false, location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location };
                                        global.kiosk.push(data);
                                        global.worklistGroup.push(group);
                                        global.io.emit('kiosk_hn', data);
                                    }else 
                                    if (res_getappointment.success && res_getappointment.data != '') {
                                        var group = '2';
                                        var vscard1 = card.substr(0, 4, card);
                                        var vscard2 = card.substr(4, 5, card);
                                        var vscard3 = card.substr(9, 4, card);

                                        var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                        var citizenid_xxx = vscard1 + vscard4 + vscard3;
                                        var datainfo = { hnno: hn, gender: gender, patient_uid: res_chkidcard.data.uid, idcard: card, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: res_chkidcard.payorid, type: res_chkidcard.patienttypeid, payorname: '', payorcode: '' };
                                        dataResult.push(datainfo);
                                        var data = { location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location };
                                        global.kiosk.push(data);
                                        global.worklistGroup.push(group);
                                        global.io.emit('kiosk_hn', data);
                                        ////console.logglobal.kiosk, "TEST MOCK DATA ACTIVE=N")
                                        res.status(200).send({ success: true, idcard: card, worklistgroup: group, step: 'next6', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location });
                                    } else {
                                        var group = '1';
                                        var vscard1 = card.substr(0, 4, card);
                                        var vscard2 = card.substr(4, 5, card);
                                        var vscard3 = card.substr(9, 4, card);

                                        var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                        var citizenid_xxx = vscard1 + vscard4 + vscard3;
                                        var datainfo = { hnno: hn, gender: gender, patient_uid: res_chkidcard.data.uid, idcard: card, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: res_chkidcard.payorid, type: res_chkidcard.patienttypeid, payorname: '', payorcode: '' };
                                        dataResult.push(datainfo);
                                        var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next6', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location };
                                        global.kiosk.push(data);
                                        global.worklistGroup.push(group);
                                        global.io.emit('kiosk_hn', data);
                                        ////console.logglobal.kiosk, "TEST MOCK DATA ACTIVE=N")
                                        res.status(200).send({ success: true, idcard: card, worklistgroup: group, step: 'next6', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location });

                                        ////console.logcard, '1');
                                    }
                                });

                            }

                        } else {
                            var dataResult = [];
                            var fullHn = '';
                            var s = hn.split('/');
                            fullHn = s[1] + '/' + s[0];
                            //console.log(fullHn, "TEST HN SMART")
                            getAppointmentDataAPI({ hnnumber: fullHn, dateappointment: datetime }, (res_getappointment) => {
                                if(res_getappointment.error == 500 || res_getappointment.error == 404){
                                    // res.status(200).send({ success: false, error:500, api_his:true, api_appointment:false, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                    var group = '1';

                                    var vscard1 = idcard.substr(0, 4, idcard);
                                    var vscard2 = idcard.substr(4, 5, idcard);
                                    var vscard3 = idcard.substr(9, 4, idcard);

                                    var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                    var citizenid_xxx = vscard1 + vscard4 + vscard3;

                                    var datainfo = { hnno: hn, gender: gender, patient_uid: res_chkidcard.data.uid, idcard: card, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: res_chkidcard.payorid, type: res_chkidcard.patienttypeid, payorname: '', payorcode: '' };
                                    dataResult.push(datainfo);
                                    var data = {success: false, error:500, api_his:true, api_appointment:false, location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location };
                                    global.kiosk.push(data);
                                    global.worklistGroup.push(group);
                                    global.io.emit('kiosk_hn', data);
                                }else
                                if (res_getappointment.success && res_getappointment.data != '') {
                                    var group = '2';
                                    //console.log(res_getappointment, "DATA FROM API APPOINTMENT 2")
                                    var vscard1 = idcard.substr(0, 4, idcard);
                                    var vscard2 = idcard.substr(4, 5, idcard);
                                    var vscard3 = idcard.substr(9, 4, idcard);

                                    var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                    var citizenid_xxx = vscard1 + vscard4 + vscard3;

                                    var datainfo = { hnno: hn, gender: gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                    dataResult.push(datainfo);
                                    var data = { location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };

                                    global.io.emit('kiosk_hn', data);
                                    global.kiosk.push(data);
                                    global.worklistGroup.push(group);
                                    res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                } else {
                                    ////console.log'NO INFO FROM ID CARD')
                                    var group = '1';
                                    var dataResult = [];
                                    var vscard1 = idcard.substr(0, 4, idcard);
                                    var vscard2 = idcard.substr(4, 5, idcard);
                                    var vscard3 = idcard.substr(9, 4, idcard);

                                    var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                    var citizenid_xxx = vscard1 + vscard4 + vscard3;
                                    var datainfo = { hnno: hn, gender: gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                    dataResult.push(datainfo);
                                    var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next6', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location };

                                    global.io.emit('kiosk_hn', data);
                                    global.kiosk.push(data);
                                    global.worklistGroup.push(group);
                                    res.status(200).send({ success: true, idcard: card, worklistgroup: group, step: 'next6', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location });

                                    // ////console.log'NEW PATIENT')
                                    // var group = '3';
                                    // var idcard = [];
                                    // var datacard = {idcard:card};
                                    // var dataResult = [];
                                    // idcard.push(datacard);
                                    // ////console.logidcard, 'TT');

                                    // var datainfo = { hnno: hn, idcard: idcard, prefixname_th: '', firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };

                                    // ////console.logdatainfo, 'NEW PATIENT')
                                    // dataResult.push(datainfo);
                                    // var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next2', scan: false, from_lc: '1' };
                                    // global.kiosk.push(data);
                                    // global.worklistGroup.push(group);
                                    // ////console.logglobal.kiosk, 'TEst 3');
                                    // ////console.logfindIndex(1));
                                    // res.status(200).send({ success: true, idcard: card, worklistgroup: group, step: 'next2', scan: false, from_lc: '1' });

                                    // ////console.logcard, '3');
                                }
                            });
                        }
                    });
                    // } else {

                    // }
                } else {
                    chkIdcardFromDB({ hn: '', idcard: card, firstName: '', lastName: '', birthDate: '' }, (res_chkidcard) => {
                        //console.log(res_chkidcard.data, "TEST CHK PATIENT FROM ID CARD")
                        // //console.log(req.body[0].firstname_th, "TEST REQ BODY")
                        if (res_chkidcard.success) {
                            // //console.log("TEST CHK PATIENT FROM ID CARD 1")
                            if (res_chkidcard.data.active == 'Y') {
                                ////console.log'FROM PATIENT DB')
                                var group = '3';
                                getPatientRePrint({ uid: res_chkidcard.data.uid, group: res_chkidcard.data.worklistgroupuid }, (res_patientinfo) => {
                                    //console.logres_patientinfo, "TEST REPRINT 1")
                                    if (res_patientinfo.success) {
                                        var datainfo = '';
                                        for (var i = 0; i < res_patientinfo.data.length; i++) {
                                            var flownameRoom = '';
                                            // dataResult = res_patientinfo.data[i];
                                            ////console.logres_patientinfo.data[i], "TEST REPRINT 5")
                                            if (res_patientinfo.data[i].prename != 'null') {
                                                prename = res_patientinfo.data[i].prename;
                                            } else {
                                                prename = req.body[0].prefixname_th;
                                            }

                                            if (res_patientinfo.data[i].forename != 'null') {
                                                forename = res_patientinfo.data[i].forename;
                                            } else {
                                                forename = '';
                                            }

                                            if (res_patientinfo.data[i].surname != 'null') {
                                                surname = res_patientinfo.data[i].surname;
                                            } else {
                                                surname = '';
                                            }

                                            if (res_patientinfo.data[i].dob != 'null') {
                                                dob = res_patientinfo.data[i].dob;
                                            } else {
                                                dob = '';
                                            }

                                            if (res_patientinfo.data[i].hn != 'null') {
                                                var hnno = res_patientinfo.data[i].hn;
                                            } else {
                                                var hnno = '';
                                            }

                                            if (res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != '') {
                                                flownameRoom = ' ' + res_patientinfo.data[i].clinicname.replace(".", ",");
                                            }

                                            var ptuid = res_patientinfo.data[i].patientuid;
                                            var vscard1 = res_patientinfo.data[i].idcard.substr(0, 4, res_patientinfo.data[i].idcard);
                                            var vscard2 = res_patientinfo.data[i].idcard.substr(4, 5, res_patientinfo.data[i].idcard);
                                            var vscard3 = res_patientinfo.data[i].idcard.substr(9, 4, res_patientinfo.data[i].idcard);

                                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                            var citizenid_xxx = vscard1 + vscard4 + vscard3;
                                            datainfo = { hnno: hnno, gender: gender, idcard: res_patientinfo.data[i].idcard, citizenid_xxx: citizenid_xxx, prefixname_th: prename, firstname_th: forename, lastname_th: surname, issuedate: req.body[0].dob, patient_uid: ptuid };
                                            var dddd = {
                                                "location": '8',
                                                "sequence": i + 1,
                                                "title": res_patientinfo.data[i].title,
                                                "flowname": res_patientinfo.data[i].flowname + flownameRoom,
                                                "waiting": "",
                                                "queueno": res_patientinfo.data[i].queueno,
                                                "worklistuid": res_patientinfo.data[i].worklistuid,
                                                "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                                                "infoData": datainfo,
                                                "group": res_patientinfo.data[i].worklistgroup,
                                                "closeselectone": res_patientinfo.data[i].closeselectone,
                                                "closevs": res_patientinfo.data[i].closevs,
                                                "closenewhn": res_patientinfo.data[i].closenewhn,
                                                "closepayor": res_patientinfo.data[i].closepayor,
                                                "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                                                "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                                                "cancelregister": res_patientinfo.data[i].cancelregister,
                                                "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                                                "kiosk_location": req.body[0].kiosk_location
                                            }
                                            global.endkiosk.push(dddd);
                                            if (i == res_patientinfo.data.length - 1) {
                                                res.status(200).send({ success: true, data: global.endkiosk, step: 'next8scan', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location });
                                                global.io.emit('kiosk_hn', { data: global.endkiosk, step: "next8scan", kiosk_location: req.body[0].kiosk_location });
                                            }
                                        }


                                    }

                                });
                            } else {
                                var group = '3';
                                var dataResult = [];
                                var prename = '';
                                var forename = '';
                                var surname = '';
                                var dob = '';
                                var hn = '';
                                var payorid = '';
                                var patienttypeid = '';
                                ////console.logres_chkidcard.data, "TEST OLD NO OLD API")
                                if (res_chkidcard.data.prename != 'null') {
                                    prename = req.body[0].prefixname_th;
                                } else {
                                    prename = req.body[0].prefixname_th;
                                }

                                if (res_chkidcard.data.forename != 'null') {
                                    forename = req.body[0].firstname_th;
                                } else {
                                    forename = req.body[0].firstname_th;
                                }

                                if (res_chkidcard.data.surname != 'null') {
                                    surname = req.body[0].lastname_th;
                                } else {
                                    surname = req.body[0].lastname_th;
                                }

                                if (res_chkidcard.data.dob != 'null') {
                                    dob = req.body[0].dob;
                                } else {
                                    dob = req.body[0].dob;
                                }

                                if (res_chkidcard.data.hn != 'null') {
                                    hn = res_chkidcard.data.hn;
                                } else {
                                    hn = '';
                                }

                                if (res_chkidcard.data.payorid != 'null') {
                                    payorid = res_chkidcard.data.payorid;
                                } else {
                                    payorid = '';
                                }

                                if (res_chkidcard.data.patienttypeid != 'null') {
                                    patienttypeid = res_chkidcard.data.patienttypeid;
                                } else {
                                    patienttypeid = '';
                                }
                                // var Patientold = { PatientNewOrNoHnc: 'PatientOld' };
                                var vscard1 = card.substr(0, 4, card);
                                var vscard2 = card.substr(4, 5, card);
                                var vscard3 = card.substr(9, 4, card);

                                var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                var citizenid_xxx = vscard1 + vscard4 + vscard3;
                                var datainfo = { hnno: hn, gender: gender, idcard: card, citizenid_xxx: citizenid_xxx, prefixname_th: prename, firstname_th: forename, lastname_th: surname, issuedate: dob, payor: payorid, type: patienttypeid, payorname: '', payorcode: '' };
                                dataResult.push(datainfo);
                                var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next2', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location };
                                global.kiosk.push(data);
                                global.io.emit('kiosk_hn', data);
                                res.status(200).send({ success: true, idcard: res_chkidcard.data.idcard, worklistgroup: group, step: 'next2', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location });

                                // ////console.logidcard, '1');
                            }

                        } else {
                            // ////console.log'NO INFO FROM ID CARD')
                            ////console.log'NEW PATIENT')
                            //console.log("TEST CHK PATIENT FROM ID CARD 2");
                            hnno = '';
                            payorid = '';
                            type = '';
                            idcard = req.body[0].citizenid;
                            prefixname_th = req.body[0].prefixname_th;
                            firstname_th = req.body[0].firstname_th;
                            lastname_th = req.body[0].lastname_th;
                            issuedate = req.body[0].dob;
                            payorname = '';
                            payorcode = '';

                            var group = '3';
                            // var idcard = [];
                            // var datacard = { idcard: card };
                            var dataResult = [];
                            // idcard.push(datacard);
                            // ////console.logidcard, 'TT');
                            var vscard1 = card.substr(0, 4, card);
                            var vscard2 = card.substr(4, 5, card);
                            var vscard3 = card.substr(9, 4, card);

                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                            var citizenid_xxx = vscard1 + vscard4 + vscard3;

                            var datainfo = { hnno: hnno, gender: gender, idcard: card, citizenid_xxx: citizenid_xxx, prefixname_th: prefixname_th, firstname_th: firstname_th, lastname_th: lastname_th, issuedate: issuedate, payor: payorid, type: '', payorname: '', payorcode: '' };

                            ////console.logdatainfo, 'NEW PATIENT')
                            dataResult.push(datainfo);
                            var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next2', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location };
                            global.kiosk.push(data);
                            global.worklistGroup.push(group);
                            ////console.logglobal.kiosk, 'TEst 3');
                            ////console.logfindIndex(1));
                            res.status(200).send({ success: true, idcard: idcard, worklistgroup: group, step: 'next2', scan: true, from_lc: '1', kiosk_location: req.body[0].kiosk_location });

                            global.io.emit('kiosk_hn', data);
                            ////console.logidcard, '3');
                        }

                    });
                }
            }

        });
    } else {
        ////console.log'False Data')
        res.status(200).json({ success: false })
    }
});
// 0=No Card, 1=Processing, 2=Completed
router.post('/smartcard/status', (req, res) => {
    if (req.body.status_code == 1) {
        ////console.log'No Card !');
        global.statusscan = 3; // No Card
        global.io.emit('chk_card_insert', { scan_status: global.statusscan, kiosk_location: req.body.kiosk_location });
        global.io.emit('status_processing', { scan_status: global.statusscan, kiosk_location: req.body.kiosk_location });
        // global.io.emit('status-smartcard', {status:req.body.status_code, message:req.body.message});
        // global.io.emit('scan_station', { step: 'next1' });
    } else if (req.body.status_code == 2) {
        ////console.log'Card Insert !');
        global.statusscan = 1; // Card Insert
        global.io.emit('chk_card_insert', { scan_status: global.statusscan, kiosk_location: req.body.kiosk_location });
        global.io.emit('status_processing', { scan_status: global.statusscan, kiosk_location: req.body.kiosk_location });

        // global.io.emit('scan_station', { step: 'next1' });

        // global.io.emit('status-smartcard', {status:req.body.status_code, message:req.body.message});
    } else if (req.body.status_code == 3) {
        ////console.log'Processing Detail !');
        // global.io.emit('status-smartcard', {status:req.body.status_code, message:req.body.message});
    } else if (req.body.status_code == 5) {
        ////console.log'Processing Image !');
        //status 3
        // global.io.emit('status-smartcard', {status:req.body.status_code, message:req.body.message});
        ////console.logreq.body.message);
    } else if (req.body.status_code == 6) {
        ////console.log'Completed !');
        // ////console.logreq.body)
        global.statusscan = 2; // Card Completed
        global.io.emit('chk_card_insert', { scan_status: global.statusscan, idcard: req.body.citizenid, kiosk_location: req.body.kiosk_location });
        global.io.emit('status_processing', { scan_status: global.statusscan, kiosk_location: req.body.kiosk_location });
        // global.io.emit('status-smartcard', {status:req.body.status_code, message:req.body.message});
    } else if (req.body.status_code == 101) {
        ////console.log'Error Scan');
        global.statusscan = 0; // No Card
        global.io.emit('chk_card_insert', { scan_status: global.statusscan, kiosk_location: req.body.kiosk_location });
        global.io.emit('status_processing', { scan_status: global.statusscan, kiosk_location: req.body.kiosk_location });
        // global.io.emit('status-smartcard', {status:req.body.status_code, message:req.body.message});
        // global.io.emit('scan_station', { step: 'next1' });
    }

    res.status(200).json({ success: true });

});
// ------------------------ End API Smart Card From Scan


// HN

// END HN

// NOHN (Scan & Input)
router.post('/smartcard/scannohn', (req, res) => {
    var data = req.body;
    //console.logdata, 'TEST HAVE HN')
    ////console.logglobal.kiosk, 'data');
    if (req.body.location == '1' && req.body.step == 'next2' && req.body.scan == 'false') {
        // res.status(200).send({success:true,scan:false});
        ////console.logreq.body);
        ////console.logglobal.kiosk[findIndex(1)], 'test data userinfo1')
        // global.io.emit('kiosk_nohn', { cause: '1',location: '1', data: global.kiosk[findIndex(1, req.body.kiosk_location)], worklistgroup: global.worklistGroup[findIndex(1, req.body.kiosk_location)], scan: false, kiosk_location: data.kiosk_location });
        res.status(200).send({ cause: '1',location: '1', data: global.kiosk[findIndex(1, req.body.kiosk_location)], worklistgroup: global.worklistGroup[findIndex(1, req.body.kiosk_location)], scan: false, kiosk_location: data.kiosk_location });
    } else if (req.body.location == '1' && req.body.step == 'next2') {
        ////console.logglobal.kiosk[findIndex(1)], 'test data userinfo2')
        // global.io.emit('kiosk_nohn', { cause: '2',location: '1', data: global.kiosk[findIndex(1, req.body.kiosk_location)], worklistgroup: global.worklistGroup[findIndex(1, req.body.kiosk_location)], scan: true, kiosk_location: data.kiosk_location });
        res.status(200).send({ cause: '2',location: '1', data: global.kiosk[findIndex(1, req.body.kiosk_location)], worklistgroup: global.worklistGroup[findIndex(1, req.body.kiosk_location)], scan: true, kiosk_location: data.kiosk_location });
        // res.status(200).send({success:true,scan:true});
    } else {
        global.io.emit('kiosk_nohn', { cause: '3'});

        res.status(201).send();
        // res.status(200).send({ success: false, scan: false, kiosk_location: req.body.kiosk_location });
    }
    // clearKioskLocation(req.body.kiosk_location);
});
// END NOHN (Scan & Input)


var CHKPATIENT = (idcard, callback) => {
    var sql = "SELECT * FROM tr_patient WHERE idcard='" + idcard + "' AND cwhen::date = now()::date";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                callback({ success: true, data: results.rows[0] })
            } else {
                callback({ success: false })
            }

        } else {
            callback({ success: false })
        }
    });
}

var getPatientRePrint = (param, callback) => {
    ////console.logparam, "PARAMETER")
    var sql = '';
    // sql = "select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,max(queueno) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title from tr_patient c LEFT JOIN tr_patientqueue a ON c.uid = a.patientuid left join vw_worklistprocess b on(b.worklistgroup = '" + param.group + "' and(b.groupprocessuid = a.groupprocessuid or b.groupprocessuid is null)) where c.uid = '" + param.uid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle Order by worklistuid"
    sql = "select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,(select queueno from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and tr_patientqueue.groupprocessuid = max(b.groupprocessuid) limit 1) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title from vw_worklistprocess b LEFT outer JOIN tr_patient c on b.worklistgroup = '" + param.group + "' left outer join tr_patientqueue a ON c.uid = a.patientuid where c.uid = '" + param.uid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle,a.groupprocessuid Order by worklistuid";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                callback({ success: true, data: results.rows })
            } else {
                callback({ success: false, data: '' })
            }
        }
    });
}

// var chkApiOldPatient = (param, callback) => {
//     //console.log(param, "CHK HN OLD 5321321")
//     var test = 'http://27.254.59.21/ords/pmkords/hlab/patient/' + param.hn + '/' + param.idcard;
//     //console.logtest)
//     axios({
//         method: 'get',
//         url: 'http://191.123.58.33:9000/ords/pmkords/hlab/patient/' + param.hn + '/' + param.idcard,
//         headers: {
//             "Content-Type": "application/json",
//             "Access-Control-Allow-Origin": "*",
//             "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
//             "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
//             "Authorization": "Basic YXJtOjEyMzQ",
//         },

//     }).then(function (response) {
//         // var dataInfoHIS = '';
//         // var aaa = { success: true, data: '' };
//         // for (var i = 0; i < response.data.length; i++) {
//         //     if (response.data[i].hn == param.hn || response.data[i].pid == param.idcard) {
//         //         aaa = { success: true, data: response.data[i] }
//         //     }
//         // }
//         //console.log(response.data)
//         // callback(aaa);
//         if (response.data) {
//             callback({ success: true, data: response.data.data })
//         } else {
//             callback({ success: true, data: '' });
//         }

//     }).catch(error => {
//         //console.log(error.response.status, "ERROR API FROM OLD PATIENT WITH INPUT")
//         if (error.response.status == 404) {
//             callback({ success: true, data: '' });
//         } else if (error.response.status == 500) {
//             callback({ success: true, error: 500 });
//         }
//     });
// }


var chkApiOldPatient = (param, callback) => {
    axios({
        method: 'get',
        url: 'http://27.254.59.21:4001/ords/pmkords/hlab/patient/' + param.hn + '/' + param.idcard,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        pid: param

    }).then(function (response) {
        // var dataInfoHIS = '';
        // //console.logresponse.HTTP, "TEST MOCK HIS")
        // callback(aaa);
        if (response.data) {
            callback({ success: true, data: response.data })
        } else {
            callback({ success: true, data: '' });
        }

    }).catch(function (error) {
        //console.logerror.response.status, "ERROR API FROM OLD PATIENT WITH INPUT")
        if(error.response){
            if (error.response.status == 404) {
                callback({ success: true, data: '' });
            } else if (error.response.status == 500) {
                callback({ success: true, error: 500 });
            }
        }else{
            callback({ success: true, error: 500 });
        }
    });
}

var chkIdcardFromDB = (data, callback) => {
    ////console.logdata.idcard, "CHK ID CARD PARAMETER")
    var sql = "SELECT * FROM tr_patient WHERE idcard = '" + data.idcard + "' AND cwhen::date = now()::date ORDER BY uid DESC LIMIT 1";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                callback({ success: true, data: results.rows[0] })
            } else {
                callback({ success: true, data: '' })
            }
        }
    });
}

// var getAppointmentDataAPI = (param, callback) => {
//     //console.log(param, 'parameter appointment')
//     // var productionurl = `http://27.254.59.21/ords/pmkords/hlab/appointment/${param.dateappointment}/null/${param.hnnumber}`;
//     var productionurl = `http://191.123.58.33:9000/ords/pmkords/hlab/appointments/${param.dateappointment}/null/${param.hnnumber}`;
//     //console.log(productionurl)
//     axios({
//         method: 'get',
//         url: productionurl,
//         headers: {
//             "Content-Type": "application/json",
//             "Access-Control-Allow-Origin": "*",
//             "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
//             "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
//             "Authorization": "Basic YXJtOjEyMzQ",
//         },

//     }).then(function (response) {
//         //console.log(response.data, "APPOINTMENT")
//         if (response.data) {
//             callback({ success: true, data: response.data });
//         } else {
//             callback({ success: true, data: '' });
//         }
//     }).catch(error => {
//         //console.log(error);
//         if (error.response.status == 404) {
//             callback({ success: true, data: '' });
//         } else if (error.response.status == 500) {
//             callback({ success: true, error: 500 });
//         }
//         // ////console.log(err.response.status, "ERROR API FROM APPOINTMENT WITH INPUT")
//     });
// }

var getAppointmentDataAPI = (param, callback) => {
    //console.log(param, 'parameter appointment')
    var productionurl = `http://191.123.95.38:818/ords/pmkords/hlab/appointment/${param.dateappointment}/null/${param.hnnumber}`;
    axios({
        method: 'get',
        url: productionurl,
        headers: {
            "Content-Type": "application/json"
        },
        timeout: 3000,
    }).then(function (response) {
        // console.log(response, "RESPONSE")
        if (response.data) {
            callback({ success: true, data: response.data });
            //console.log(response.data);
        } else {
            callback({ success: true, data: '' });
            //console.log('2');
        }
    }).catch(error => {
        // console.log(error, "ERROR");
        if(error.response == undefined){
            // console.log('5')
            callback({ success: true, error: 500 });
        }else if(error.response != undefined){
            // console.log('6')
            if (error.response.status == 404) {
                callback({ success: true, data: '', error: 404});
            } else if (error.response.status == 500) {
                callback({ success: true, error: 500 });
            }
        }
        // //console.logerr.response.status, "ERROR API FROM APPOINTMENT WITH INPUT")
    });
}

var clearEndkioskLocation = (param) => {
    const arr1 = global.endkiosk.filter((d) => {
        return d.kiosk_location != param;
    });
    global.endkiosk = arr1;
}

var clearKioskLocation = (param) => {
    const arr1 = global.kiosk.filter((d) => {
        return d.kiosk_location != param;
    });
    global.kiosk = arr1;
}

module.exports = router;