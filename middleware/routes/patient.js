const router = require('express').Router();
const { Pool } = require('pg');
const findIndex = require('./../function/findIndex');
const deleteIndexAll = require('./../function/deleteIndexAll');
const config = require('./../function/connect');
var dateFormat = require('dateformat');

const axios = require('axios');
const pool = new Pool(config);

router.post('/patient', (req, res) => {
    console.log(req.body, "id card");
    // Find INDEX
    var index = findIndex(1, req.body.kiosk_location);
    if (index != -1) {
        global.kiosk.splice(index, 1);
    }
    var d = new Date();
    var now = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
    var datetime = dateFormat(now, "yyyy-mm-dd");
    ////console.logdatetime, 'date now appointment')

    // res.setHeader('Content-Type', 'text/plain');
    // res.setHeader('Content-Type', 'multipart/form-data');
    res.setHeader('Content-Type', 'application/json')

    var card = req.body.idcard;
    //console.logreq.body.idcard, "id card");
    // var str = 'ทดสอบ / 555';

    var ref = '';
    if (card.includes('r')) {
        ref = card;
    }




    if (card.length == 13 && ref == '') {
        var hnnull = null;
        var hnnull2 = null;
        // //console.log(hnnull+'/'+hnnull2)
        chkApiOldPatient({ hn: hnnull + '/' + hnnull2, idcard: card }, (res_chkoldpatient) => { 
            if(res_chkoldpatient.error){
                res.status(200).send({ success: false, error:500, api_his:false, api_appointment: true, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
            }else{
                if(typeof res_chkoldpatient.data[0] != 'undefined'){
                    res_chkoldpatient.data = res_chkoldpatient.data[0];
                }
                var hn = res_chkoldpatient.data.hn;
                var idcard = res_chkoldpatient.data.pid;
                var preName = res_chkoldpatient.data.preName;
                if (preName == '' || preName == undefined || preName == null) {
                    preName = 'คุณ';
                }
                var firstName = res_chkoldpatient.data.firstName;
                var lastName = res_chkoldpatient.data.lastName;                    

                var gender = '';
                // console.log(res_chkoldpatient.data, 'TEST GENDER 1')
                if(res_chkoldpatient.data.gender){
                    if(res_chkoldpatient.data.gender != '' || res_chkoldpatient.data.gender != null || typeof(res_chkoldpatient.data.gender != undefined)){
                        gender = res_chkoldpatient.data.gender;
                        // console.log(gender, 'TEST GENDER 2')
                    }
                }else{
                    gender = '';
                }
                //console.log(res_chkoldpatient, 'OLD')
                if (res_chkoldpatient.success && res_chkoldpatient.data != '') {

                    var birth = res_chkoldpatient.data.birthDate;
                    var bd = '';
                    var birthDate = '';
                    var fullbirth = '';
                    if (birth != null) {
                        if (birth.includes('-')) {
                            bd = birth.split('-');
                        }
                        //console.log(bd, 'birth dateee');
                        fullbirth = (parseInt(bd[0]) + 543) + '-' + bd[1] + '-' + bd[2];
                        birthDate = dateFormat(fullbirth, `dd/mm/yyyy`);
                    }
                    ////console.logres_chkoldpatient, "TEST API CHK OLD PATIENT");
                    // if (res_chkoldpatient.pid == card) {

                    ////console.logcard, '1');

                    chkIdcardFromDB({ hn: hn, idcard: idcard, firstName: firstName, lastName: lastName, birthDate: birthDate }, (res_chkidcard) => {
                        ////console.logres_chkidcard.data, "TEST CHK PATIENT FROM ID CARD")
                        if (res_chkidcard.success) {
                            if (res_chkidcard.data.active == 'Y') {
                                var group = '1';
                                ////console.log{ uid: res_chkidcard.data.uid, group: group }, "TEST 1111111111111111111")
                                getPatientRePrint({ uid: res_chkidcard.data.uid, group: res_chkidcard.data.worklistgroupuid }, (res_patientinfo) => {
                                    ////console.logres_patientinfo, "TEST REPRINT")
                                    if (res_patientinfo.success) {
                                        var datainfo = '';
                                        clearKioskLocation(req.body.kiosk_location);
                                        for (var i = 0; i < res_patientinfo.data.length; i++) {
                                            var flownameRoom = '';
                                            // dataResult = res_patientinfo.data[i];
                                            // ////console.logdataResult, "TEST REPRINT")
                                            var vscard1 = res_patientinfo.data[i].idcard.substr(0, 4, res_patientinfo.data[i].idcard);
                                            var vscard2 = res_patientinfo.data[i].idcard.substr(4, 5, res_patientinfo.data[i].idcard);
                                            var vscard3 = res_patientinfo.data[i].idcard.substr(9, 4, res_patientinfo.data[i].idcard);

                                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                            var citizenid_xxx = vscard1 + vscard4 + vscard3;
                                            
                                            if (res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != '') {
                                                flownameRoom = ' ' + res_patientinfo.data[i].clinicname.replace(".", ",");
                                            }
                                            console.log('genderzaza');
                                            console.log(gender);
                                            datainfo = { hnno: hn, idcard: idcard,gender:gender, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, patient_uid: res_patientinfo.data[i].patientuid};

                                            var dddd = {
                                                "location": '1',
                                                "sequence": i + 1,
                                                "title": res_patientinfo.data[i].title,
                                                "flowname": res_patientinfo.data[i].flowname+flownameRoom,
                                                "waiting": "",
                                                "queueno": res_patientinfo.data[i].queueno,
                                                "worklistuid": res_patientinfo.data[i].worklistuid,
                                                "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                                                "infoData": datainfo,
                                                "clinicname": res_patientinfo.data[i].clinicname,
                                                "group": res_patientinfo.data[i].worklistgroup,
                                                "closeselectone": res_patientinfo.data[i].closeselectone,
                                                "closevs": res_patientinfo.data[i].closevs,
                                                "closenewhn": res_patientinfo.data[i].closenewhn,
                                                "closepayor": res_patientinfo.data[i].closepayor,
                                                "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                                                "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                                                "cancelregister": res_patientinfo.data[i].cancelregister,
                                                "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                                                "kiosk_location": req.body.kiosk_location
                                            }
                                            global.endkiosk.push(dddd);
                                            if (i == res_patientinfo.data.length - 1) {
                                                console.log("CHECK1");
                                                res.status(200).send({ success: true, data: global.endkiosk, step: 'next8', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });

                                            }
                                        }


                                    }

                                });
                            } else {
                                // var group = '1';
                                var dataResult = [];
                                var fullHn = '';
                                var s = hn.split('/');
                                fullHn = s[1] + '/' + s[0];
                                getAppointmentDataAPI({ hnnumber: fullHn, dateappointment: datetime }, (res_getappointment) => {
                                    if(res_getappointment.error == 500 || res_getappointment.error == 404){
                                        console.log("CHECK2");
                                        res.status(200).send({ success: false, error:500, api_his:true, api_appointment:false, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                    }
                                    if (res_getappointment.success && res_getappointment.data != '') {
                                        var group = '2';
                                        //console.log(res_getappointment, "DATA FROM API APPOINTMENT 1")
                                        var vscard1 = idcard.substr(0, 4, idcard);
                                        var vscard2 = idcard.substr(4, 5, idcard);
                                        var vscard3 = idcard.substr(9, 4, idcard);

                                        var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                        var citizenid_xxx = vscard1 + vscard4 + vscard3;

                                        var datainfo = { hnno: hn, gender:gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                        dataResult.push(datainfo);
                                        var data = { location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                        global.kiosk.push(data);
                                        global.worklistGroup.push(group);
                                        console.log("CHECK3");
                                        res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                    } else {
                                        var group = '1';
                                        var vscard1 = idcard.substr(0, 4, idcard);
                                        var vscard2 = idcard.substr(4, 5, idcard);
                                        var vscard3 = idcard.substr(9, 4, idcard);

                                        var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                        var citizenid_xxx = vscard1 + vscard4 + vscard3;

                                        var datainfo = { hnno: hn, gender:gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                        dataResult.push(datainfo);
                                        var data = { location: '1', data: dataResult, dataAppointment: '', worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                        global.kiosk.push(data);
                                        global.worklistGroup.push(group);
                                        //console.log"TEST API")
                                        console.log("CHECK4");
                                        res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                    }
                                });
                                // var datainfo = { hnno: hn, idcard: idcard, prefixname_th: 'คุณ', firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                // dataResult.push(datainfo);
                                // var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next6', scan: false, from_lc: '1' };
                                // global.kiosk.push(data);
                                // global.worklistGroup.push(group);
                                // res.status(200).send({ success: true, idcard: card, worklistgroup: group, step: 'next6', scan: false, from_lc: '1' });

                                // ////console.logcard, '1');
                            }

                        } else {
                            ////console.log'NO INFO FROM ID CARD')
                            var dataResult = [];
                            var fullHn = '';
                            var s = hn.split('/');
                            fullHn = s[1] + '/' + s[0];
                            getAppointmentDataAPI({ hnnumber: fullHn, dateappointment: datetime }, (res_getappointment) => {
                                if(res_getappointment.error == 500 || res_getappointment.error == 404){
                                    console.log("CHECK5");
                                    res.status(200).send({ success: false, error:500, api_his:true, api_appointment:false, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                    //res.status(200).send({ success: false, error:500, api_his:true, api_appointment:false, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                }
                                if (res_getappointment.success && res_getappointment.data != '') {
                                    var group = '2';
                                    //console.log(res_getappointment, "DATA FROM API APPOINTMENT 2")
                                    var vscard1 = idcard.substr(0, 4, idcard);
                                    var vscard2 = idcard.substr(4, 5, idcard);
                                    var vscard3 = idcard.substr(9, 4, idcard);

                                    var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                    var citizenid_xxx = vscard1 + vscard4 + vscard3;

                                    var datainfo = { hnno: hn, gender:gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                    dataResult.push(datainfo);
                                    var data = { location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                    global.kiosk.push(data);
                                    global.worklistGroup.push(group);
                                    if(res_getappointment.error != 500 && res_getappointment.error != 404){
                                        console.log("CHECK6");
                                        res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                    }
                                } else {
                                    var group = '1';
                                    var vscard1 = idcard.substr(0, 4, idcard);
                                    var vscard2 = idcard.substr(4, 5, idcard);
                                    var vscard3 = idcard.substr(9, 4, idcard);

                                    var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                    var citizenid_xxx = vscard1 + vscard4 + vscard3;

                                    var datainfo = { hnno: hn, gender:gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                    dataResult.push(datainfo);
                                    var data = { location: '1', data: dataResult, dataAppointment: '', worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                    global.kiosk.push(data);
                                    global.worklistGroup.push(group);
                                    if(res_getappointment.error != 500 && res_getappointment.error != 404){
                                        console.log("CHECK7");
                                        res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                    }
                                }
                            });


                        }

                    });
                    // } else {

                    // }
                } else {
                    chkIdcardFromDB({ hn: '', idcard: card, firstName: '', lastName: '', birthDate: '' }, (res_chkidcard) => {
                        ////console.logres_chkidcard.data, "TEST CHK PATIENT FROM ID CARD 2")
                        if (res_chkidcard.success) {
                            if (res_chkidcard.data.active == 'Y') {
                                ////console.logres_chkidcard.data.uid, "TEST 1111111111111111111")
                                var group = '3';
                                getPatientRePrint({ uid: res_chkidcard.data.uid, group: res_chkidcard.data.worklistgroupuid }, (res_patientinfo) => {
                                    ////console.logres_patientinfo, "TEST REPRINT")
                                    if (res_patientinfo.success) {
                                        var datainfo = '';
                                        clearKioskLocation(req.body.kiosk_location);
                                        for (var i = 0; i < res_patientinfo.data.length; i++) {
                                            var flownameRoom = '';
                                            // dataResult = res_patientinfo.data[i];
                                            //console.log(res_patientinfo.data[i], "TEST REPRINT")
                                            if (res_patientinfo.data[i].prename != 'null') {
                                                prename = res_patientinfo.data[i].prename;
                                            } else {
                                                prename = '';
                                            }

                                            if (res_patientinfo.data[i].forename != 'null') {
                                                forename = res_patientinfo.data[i].forename;
                                            } else {
                                                forename = '';
                                            }

                                            if (res_patientinfo.data[i].surname != 'null') {
                                                surname = res_patientinfo.data[i].surname;
                                            } else {
                                                surname = '';
                                            }

                                            if (res_patientinfo.data[i].birthdate != '' || res_patientinfo.data[i].birthdate != undefined) {
                                                dob = res_patientinfo.data[i].birthdate;
                                            } else {
                                                dob = '';
                                            }

                                            if (res_patientinfo.data[i].hn != 'null') {
                                                hn = res_patientinfo.data[i].hn;
                                            } else {
                                                hn = '';
                                            }


                                            if (res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != '') {
                                                flownameRoom = ' ' + res_patientinfo.data[i].clinicname.replace(".", ",");
                                            }

                                            var vscard1 = res_patientinfo.data[i].idcard.substr(0, 4, res_patientinfo.data[i].idcard);
                                            var vscard2 = res_patientinfo.data[i].idcard.substr(4, 5, res_patientinfo.data[i].idcard);
                                            var vscard3 = res_patientinfo.data[i].idcard.substr(9, 4, res_patientinfo.data[i].idcard);

                                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                            var citizenid_xxx = vscard1 + vscard4 + vscard3;

                                            datainfo = { hnno: hn, idcard: res_patientinfo.data[i].idcard, citizenid_xxx: citizenid_xxx, prefixname_th: prename, firstname_th: forename, lastname_th: surname, issuedate: dob, patient_uid: res_patientinfo.data[i].patientuid };
                                            var dddd = {
                                                "location": '1',
                                                "sequence": i + 1,
                                                "title": res_patientinfo.data[i].title,
                                                "flowname": res_patientinfo.data[i].flowname+flownameRoom,
                                                "waiting": "",
                                                "queueno": res_patientinfo.data[i].queueno,
                                                "worklistuid": res_patientinfo.data[i].worklistuid,
                                                "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                                                "infoData": datainfo,
                                                "group": res_patientinfo.data[i].worklistgroup,
                                                "closeselectone": res_patientinfo.data[i].closeselectone,
                                                "closevs": res_patientinfo.data[i].closevs,
                                                "closenewhn": res_patientinfo.data[i].closenewhn,
                                                "closepayor": res_patientinfo.data[i].closepayor,
                                                "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                                                "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                                                "cancelregister": res_patientinfo.data[i].cancelregister,
                                                "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                                                "kiosk_location": req.body.kiosk_location
                                            }

                                            //console.log(dddd, "TEST REPRINT ARRAY")
                                            global.endkiosk.push(dddd);
                                            if (i == res_patientinfo.data.length - 1) {
                                                console.log("CHECK8");
                                                res.status(200).send({ success: true, data: global.endkiosk, step: 'next8', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });

                                            }
                                        }


                                    }

                                });
                            } else {
                                var group = '3';
                                var dataResult = [];
                                var hnnoq = '';
                                ////console.log'TEST CHK PATIENT ACTIVE N')
                                if (res_chkidcard.data.hn != 'null') {
                                    hnnoq = res_chkidcard.data.hn
                                } else {
                                    hnnoq = '';
                                }
                                if (res_chkidcard.data.prename != 'null') {
                                    prename = res_chkidcard.data.prename;
                                } else {
                                    prename = '';
                                }

                                if (res_chkidcard.data.forename != 'null') {
                                    forename = res_chkidcard.data.forename;
                                } else {
                                    forename = '';
                                }

                                if (res_chkidcard.data.surname != 'null') {
                                    surname = res_chkidcard.data.surname;
                                } else {
                                    surname = '';
                                }

                                if (res_chkidcard.data.dob != 'null') {
                                    dob = res_chkidcard.data.dob;
                                } else {
                                    dob = '';
                                }

                                var vscard1 = card.substr(0, 4, card);
                                var vscard2 = card.substr(4, 5, card);
                                var vscard3 = card.substr(9, 4, card);

                                var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                var citizenid_xxx = vscard1 + vscard4 + vscard3;

                                var datainfo = { hnno: hnnoq, idcard: card, citizenid_xxx: citizenid_xxx, prefixname_th: prename, firstname_th: forename, lastname_th: surname, issuedate: dob, payor: '', type: '', payorname: '', payorcode: '' };
                                dataResult.push(datainfo);
                                var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next2', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                global.kiosk.push(data);
                                global.worklistGroup.push(group);
                                console.log("CHECK9");
                                res.status(200).send({ success: true, idcard: res_chkidcard.data.idcard, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next2', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });

                                ////console.logcard, '1');
                            }

                        } else {
                            // ////console.log'NO INFO FROM ID CARD')
                            ////console.log'NEW PATIENT')
                            var group = '3';
                            var idcard = [];
                            var datacard = { idcard: card };
                            var dataResult = [];
                            idcard.push(datacard);
                            ////console.logidcard, 'TT');

                            var vscard1 = card.substr(0, 4, card);
                            var vscard2 = card.substr(4, 5, card);
                            var vscard3 = card.substr(9, 4, card);

                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                            var citizenid_xxx = vscard1 + vscard4 + vscard3;

                            var datainfo = { hnno: hn, idcard: card, citizenid_xxx: citizenid_xxx, prefixname_th: '', firstname_th: '', lastname_th: '', issuedate: '', payor: '', type: '', payorname: '', payorcode: '', api_nocard: 'api_nocard' };

                            ////console.logdatainfo, 'NEW PATIENT')
                            dataResult.push(datainfo);
                            var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next2', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location, api_nocard: 'api_nocard' };
                            global.kiosk.push(data);
                            global.worklistGroup.push(group);
                            ////console.logglobal.kiosk, 'TEst 3');
                            ////console.logfindIndex(1));
                                        console.log("CHECK10");
                                        console.log()
                            res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next2', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });

                            ////console.logcard, '3');
                        }

                    });
                }
            }
        });
    } else if (card.length < 13 && ref == '') {
        if (card.includes('/')) {

            //console.logcard);
            var hnNum = '';
            var fullHn = '';
            var citizen = null;
            var s = card.split('/');
            fullHn = s[1] + '/' + s[0];
            //console.logfullHn, "1")
            // if (checkStr(card)) {
            //     // ////console.log 'มีเครื่องหมาย /' );
            //     hnNum = card.split('/');
            //     fullHn = hnNum[0] + hnNum[1];
            // }
            // var hnreplace = '';
            // if(card.indexOf('/')){
            //     fullHn = card.replace('/','')
            // }else{
            //     fullHn = card;
            // }
            // if(card.includes('/')){
            //     fullHn = card;
            // }else{
            //     return false;
            // }

            chkApiOldPatient({ hn: fullHn, idcard: citizen }, (res_chkoldpatient) => {
                if(res_chkoldpatient.error){
                    console.log("CHECK0");
                    res.status(200).send({ success: false, error:500, api_his:false, api_appointment: true, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                }else{

                    if(typeof res_chkoldpatient.data[0] != 'undefined'){
                        res_chkoldpatient.data = res_chkoldpatient.data[0];
                    }
                    
                    // console.log(res_chkoldpatient, "TEST ERROR OLD");
                    var hn = res_chkoldpatient.data.hn;
                    // console.log(res_chkoldpatient.data, " TEST HN from api")
                    var idcard = '';
                    if (res_chkoldpatient.data.pid) {
                        idcard = res_chkoldpatient.data.pid;
                    } else if (idcard == '' || idcard == undefined || idcard == null) {
                        idcard = '';
                    }

                    //console.log(idcard, 'test idcard')
                    var preName = res_chkoldpatient.data.preName;
                    if (preName == '' || preName == undefined || preName == null) {
                        preName = 'คุณ';
                    }
                    var firstName = res_chkoldpatient.data.firstName;
                    var lastName = res_chkoldpatient.data.lastName;
                    var gender = '';
                    // console.log(res_chkoldpatient.data, 'TEST GENDER 1')
                    if(res_chkoldpatient.data.gender){
                        if(res_chkoldpatient.data.gender != '' || res_chkoldpatient.data.gender != null || typeof(res_chkoldpatient.data.gender != undefined)){
                            gender = res_chkoldpatient.data.gender;
                            // console.log(gender, 'TEST GENDER 2')
                        }
                    }else{
                        gender = '';
                    }


                    var prename = '';
                    var forename = '';
                    var surname = '';
                    var dob = '';
                    var hnno = '';
                    var payorid = '';
                    var patienttypeid = '';
                    if (res_chkoldpatient.success && res_chkoldpatient.data != '') {
                        //console.log(res_chkoldpatient, "TEST API CHK OLD PATIENT HN 1");
                        // if (res_chkoldpatient.pid == card) {

                        var birth = res_chkoldpatient.data.birthDate;
                        var bd = '';
                        var birthDate = '';
                        var fullbirth = '';
                        if (birth != null) {
                            if (birth.includes('-')) {
                                bd = birth.split('-');
                            }
                            //console.log(bd, 'birth dateee');
                            fullbirth = (parseInt(bd[0]) + 543) + '-' + bd[1] + '-' + bd[2];
                            birthDate = dateFormat(fullbirth, `dd/mm/yyyy`);
                        }

                        chkHnNoFromDB({ hn: hn, idcard: idcard, firstName: firstName, lastName: lastName, birthDate: birthDate }, (res_chkhnno) => {
                            //console.logres_chkhnno.data, "TEST CHK PATIENT FROM HN")
                            if (res_chkhnno.success && res_chkhnno.data != '') {
                                if (res_chkhnno.data.active == 'Y') {
                                    //console.logres_chkhnno, "TEST REPRINT 5")
                                    var group = '1';
                                    //stophere
                                    getPatientRePrint({ uid: res_chkhnno.data.uid, group: res_chkhnno.data.worklistgroupuid }, (res_patientinfo) => {
                                        //console.logres_patientinfo, "TEST REPRINT 5")
                                        if (res_patientinfo.success && res_patientinfo.data != '') {
                                            var datainfo = '';
                                            clearKioskLocation(req.body.kiosk_location);
                                            for (var i = 0; i < res_patientinfo.data.length; i++) {
                                                var flownameRoom = '';
                                                // dataResult = res_patientinfo.data[i];
                                                ////console.logres_patientinfo.data[i], "TEST REPRINT 7777777777")
                                                var citizenid_xxx = '';
                                                if (idcard != '') {
                                                    var vscard1 = idcard.substr(0, 4, idcard);
                                                    var vscard2 = idcard.substr(4, 5, idcard);
                                                    var vscard3 = idcard.substr(9, 4, idcard);

                                                    var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                                    citizenid_xxx = vscard1 + vscard4 + vscard3;
                                                } else {
                                                    var vscard1 = res_patientinfo.data[i].idcard.substr(0, 4, res_patientinfo.data[i].idcard);
                                                    var vscard2 = res_patientinfo.data[i].idcard.substr(4, 5, res_patientinfo.data[i].idcard);
                                                    var vscard3 = res_patientinfo.data[i].idcard.substr(9, 4, res_patientinfo.data[i].idcard);

                                                    var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                                    citizenid_xxx = vscard1 + vscard4 + vscard3;
                                                }

                                                if(res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != ''){
                                                    flownameRoom = ' '+res_patientinfo.data[i].clinicname.replace(".",",");
                                                }
                                                
                                                // //console.logres_patientinfo.data[i].birthdate, "BIRTHDATE");
                                                datainfo = { hnno: hn, idcard: idcard, gender:gender, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, patient_uid: res_patientinfo.data[i].patientuid };
                                                console.log(datainfo);//nogender
                                                var dddd = {
                                                    "location": '1',
                                                    "sequence": i + 1,
                                                    "title": res_patientinfo.data[i].title,
                                                    "flowname": res_patientinfo.data[i].flowname+flownameRoom,
                                                    "waiting": "",
                                                    "queueno": res_patientinfo.data[i].queueno,
                                                    "worklistuid": res_patientinfo.data[i].worklistuid,
                                                    "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                                                    "infoData": datainfo,
                                                    "clinicname": res_patientinfo.data[i].clinicname,
                                                    "group": res_patientinfo.data[i].worklistgroup,
                                                    "closeselectone": res_patientinfo.data[i].closeselectone,
                                                    "closevs": res_patientinfo.data[i].closevs,
                                                    "closenewhn": res_patientinfo.data[i].closenewhn,
                                                    "closepayor": res_patientinfo.data[i].closepayor,
                                                    "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                                                    "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                                                    "cancelregister": res_patientinfo.data[i].cancelregister,
                                                    "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                                                    "kiosk_location": req.body.kiosk_location
                                                }
                                                global.endkiosk.push(dddd);
                                                if (i == res_patientinfo.data.length - 1) {
                                                    console.log("CHECK11");
                                                    res.status(200).send({ success: true, data: global.endkiosk, step: 'next8', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });

                                                }
                                            }


                                        }

                                    });
                                } else {
                                    // var group = '1';
                                    var dataResult = [];
                                    getAppointmentDataAPI({ hnnumber: fullHn, dateappointment: datetime }, (res_getappointment) => {
                                        if(res_getappointment.error == 500 || res_getappointment.error == 404){
                                            // console.log(res_getappointment, '1111')
                                        console.log("CHECK12");
                                            res.status(200).send({ success: false, error:500, api_his:true, api_appointment:false, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                        }
                                        if (res_getappointment.success && res_getappointment.data != '') {
                                            var group = '2';
                                            var citizenid_xxx = '';
                                            ////console.logres_getappointment, "DATA FROM API APPOINTMENT")
                                            if (idcard != '') {
                                                var vscard1 = idcard.substr(0, 4, idcard);
                                                var vscard2 = idcard.substr(4, 5, idcard);
                                                var vscard3 = idcard.substr(9, 4, idcard);

                                                var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                                citizenid_xxx = vscard1 + vscard4 + vscard3;
                                            } else {
                                                citizenid_xxx = '';
                                            }
                                            var datainfo = { hnno: hn, gender:gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                            dataResult.push(datainfo);
                                            var data = { location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                            global.kiosk.push(data);
                                            global.worklistGroup.push(group);
                                            if(res_getappointment.error != 500 && res_getappointment.error != 404){
                                                console.log("CHECK13");
                                                res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                            }
                                        } else {
                                            var group = '1';
                                            var citizenid_xxx = '';
                                            ////console.logres_getappointment, "DATA FROM API APPOINTMENT")
                                            if (idcard != '') {
                                                var vscard1 = idcard.substr(0, 4, idcard);
                                                var vscard2 = idcard.substr(4, 5, idcard);
                                                var vscard3 = idcard.substr(9, 4, idcard);

                                                var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                                citizenid_xxx = vscard1 + vscard4 + vscard3;
                                            } else {
                                                citizenid_xxx = '';
                                            }
                                            var datainfo = { hnno: hn, gender:gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: res_chkhnno.data.payorid, type: res_chkhnno.data.pattienttypeid, payorname: '', payorcode: '' };
                                            dataResult.push(datainfo);
                                            var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                            global.kiosk.push(data);
                                            global.worklistGroup.push(group);                                            
                                            if(res_getappointment.error != 500 && res_getappointment.error != 404){
                                                console.log("CHECK14");
                                                res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                            }
                                            ////console.logcard, '1');
                                        }
                                    });

                                }

                            } else {
                                //console.log(res_chkoldpatient, "TEST API CHK OLD PATIENT HN 2");
                                // ////console.log'NEW PATIENT')
                                // var group = '1';
                                // var idcard = [];
                                // var datacard = {idcard:card};
                                var dataResult = [];
                                // idcard.push(datacard);
                                // ////console.logidcard, 'TT');
                                getAppointmentDataAPI({ hnnumber: fullHn, dateappointment: datetime }, (res_getappointment) => {
                                    // console.log(res_getappointment, "APPOINT ERROR")
                                    if(res_getappointment.error == 500 || res_getappointment.error == 404){
                                        // console.log(res_getappointment, '2222')
                                        console.log("CHECK15");
                                        res.status(200).send({ success: false, error:500, api_his:true, api_appointment:false, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                    }
                                    if (res_getappointment.success && res_getappointment.data != '') {
                                        var group = '2';
                                        //console.logres_getappointment, "DATA FROM API APPOINTMENT")
                                        var citizenid_xxx = '';
                                        ////console.logres_getappointment, "DATA FROM API APPOINTMENT")
                                        if (idcard != '') {
                                            var vscard1 = idcard.substr(0, 4, idcard);
                                            var vscard2 = idcard.substr(4, 5, idcard);
                                            var vscard3 = idcard.substr(9, 4, idcard);

                                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                            citizenid_xxx = vscard1 + vscard4 + vscard3;
                                        } else {
                                            citizenid_xxx = '';
                                        }

                                        var datainfo = { hnno: hn, gender:gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                                        dataResult.push(datainfo);
                                        var data = { location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                        global.kiosk.push(data);
                                        global.worklistGroup.push(group);
                                        if(res_getappointment.error != 500 && res_getappointment.error != 404){
                                            console.log("CHECK16");
                                            res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                        }
                                    } else {
                                        var group = '1';
                                        var citizenid_xxx = '';
                                        ////console.logres_getappointment, "DATA FROM API APPOINTMENT")
                                        if (idcard != '') {
                                            var vscard1 = idcard.substr(0, 4, idcard);
                                            var vscard2 = idcard.substr(4, 5, idcard);
                                            var vscard3 = idcard.substr(9, 4, idcard);

                                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                            citizenid_xxx = vscard1 + vscard4 + vscard3;
                                        } else {
                                            citizenid_xxx = '';
                                        }
                                        var datainfo = { hnno: hn, gender:gender, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: preName, firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };

                                        ////console.logdatainfo, 'OLD PATIENT')
                                        dataResult.push(datainfo);
                                        var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                        global.kiosk.push(data);
                                        global.worklistGroup.push(group);
                                        ////console.logglobal.kiosk, 'TEst 3');
                                        ////console.logfindIndex(1));                                        
                                        if(res_getappointment.error != 500 && res_getappointment.error != 404){
                                            console.log("CHECK17");
                                            res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                                        }
                                    }
                                });


                                // ////console.logcard, '3');

                            }

                        });
                        // } else {

                        // }
                    } else {
                        chkHnNoFromDB({ hn: fullHn, idcard: '', firstName: '', lastName: '', birthDate: '' }, (res_chkhnno) => {
                            ////console.logres_chkhnno.data, "TEST CHK PATIENT FROM ID CARD")
                            if (res_chkhnno.success && res_chkhnno.data != '') {
                                if (res_chkhnno.data.active == 'Y') {
                                    var group = '3';
                                    getPatientRePrint({ uid: res_chkhnno.data.uid, group: group }, (res_patientinfo) => {
                                        ////console.logres_patientinfo, "TEST REPRINT")
                                        if (res_patientinfo.success) {
                                            var datainfo = '';
                                            clearKioskLocation(req.body.kiosk_location);
                                            for (var i = 0; i < res_patientinfo.data.length; i++) {
                                                var flownameRoom = '';
                                                // dataResult = res_patientinfo.data[i];
                                                // ////console.logdataResult, "TEST REPRINT")
                                                if (res_patientinfo.data[i].prename != 'null') {
                                                    prename = res_patientinfo.data[i].prename;
                                                } else {
                                                    prename = '';
                                                }

                                                if (res_patientinfo.data[i].forename != 'null') {
                                                    forename = res_patientinfo.data[i].forename;
                                                } else {
                                                    forename = '';
                                                }

                                                if (res_patientinfo.data[i].surname != 'null') {
                                                    surname = res_patientinfo.data[i].surname;
                                                } else {
                                                    surname = '';
                                                }

                                                if (res_patientinfo.data[i].dob != 'null') {
                                                    dob = res_patientinfo.data[i].dob;
                                                } else {
                                                    dob = '';
                                                }

                                                if (res_patientinfo.data[i].hn != 'null') {
                                                    hnno = res_patientinfo.data[i].hn;
                                                } else {
                                                    hnno = '';
                                                }

                                                if (res_patientinfo.data[i].payorid != 'null') {
                                                    payorid = res_patientinfo.data[i].payorid;
                                                } else {
                                                    payorid = '';
                                                }

                                                if (res_patientinfo.data[i].patienttypeid != 'null') {
                                                    patienttypeid = res_patientinfo.data[i].patienttypeid;
                                                } else {
                                                    patienttypeid = '';
                                                }

                                                if(res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != ''){
                                                    flownameRoom = ' '+res_patientinfo.data[i].clinicname.replace(".",",");
                                                }

                                                datainfo = { hnno: hnno, idcard: res_patientinfo.data[i].idcard, prefixname_th: prename, firstname_th: forename, lastname_th: surname, issuedate: dob, patient_uid: res_patientinfo.data[i].patientuid };
                                                var dddd = {
                                                    "location": '1',
                                                    "sequence": i + 1,
                                                    "title": res_patientinfo.data[i].title,
                                                    "flowname": res_patientinfo.data[i].flowname+flownameRoom,
                                                    "waiting": "",
                                                    "queueno": res_patientinfo.data[i].queueno,
                                                    "worklistuid": res_patientinfo.data[i].worklistuid,
                                                    "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                                                    "group": res_patientinfo.data[i].worklistgroup,
                                                    "infoData": datainfo,
                                                    "closeselectone": res_patientinfo.data[i].closeselectone,
                                                    "closevs": res_patientinfo.data[i].closevs,
                                                    "closenewhn": res_patientinfo.data[i].closenewhn,
                                                    "closepayor": res_patientinfo.data[i].closepayor,
                                                    "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                                                    "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                                                    "cancelregister": res_patientinfo.data[i].cancelregister,
                                                    "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                                                    "kiosk_location": req.body.kiosk_location
                                                }
                                                global.endkiosk.push(dddd);
                                                if (i == res_patientinfo.data.length - 1) {
                                                    console.log("CHECK18");
                                                    res.status(200).send({ success: true, data: global.endkiosk, step: 'next8', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });

                                                }
                                            }


                                        }

                                    });
                                } else {
                                    //console.log" NEW BUT INPUT HN")
                                    var group = '3';
                                    var dataResult = [];

                                    if (res_patientinfo.data[0].prename != 'null') {
                                        prename = res_patientinfo.data[0].prename;
                                    } else {
                                        prename = '';
                                    }

                                    if (res_patientinfo.data[0].forename != 'null') {
                                        forename = res_patientinfo.data[0].forename;
                                    } else {
                                        forename = '';
                                    }

                                    if (res_patientinfo.data[0].surname != 'null') {
                                        surname = res_patientinfo.data[0].surname;
                                    } else {
                                        surname = '';
                                    }

                                    if (res_patientinfo.data[0].dob != 'null') {
                                        dob = res_patientinfo.data[0].dob;
                                    } else {
                                        dob = '';
                                    }

                                    if (res_patientinfo.data[0].hn != 'null') {
                                        hnno = res_patientinfo.data[0].hn;
                                    } else {
                                        hnno = '';
                                    }

                                    if (res_patientinfo.data[0].payorid != 'null') {
                                        payorid = res_patientinfo.data[0].payorid;
                                    } else {
                                        payorid = '';
                                    }

                                    if (res_patientinfo.data[0].patienttypeid != 'null') {
                                        patienttypeid = res_patientinfo.data[0].patienttypeid;
                                    } else {
                                        patienttypeid = '';
                                    }

                                    var datainfo = { hnno: hnno, idcard: res_patientinfo.data[0].idcard, prefixname_th: prename, firstname_th: forename, lastname_th: surname, issuedate: dob, payor: payorid, type: patienttypeid, payorname: '', payorcode: '' };
                                    dataResult.push(datainfo);
                                    var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next2', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                                    global.kiosk.push(data);
                                    global.worklistGroup.push(group);
                                    console.log("CHECK19");
                                    res.status(200).send({ success: true, idcard: res_chkidcard.data.idcard, worklistgroup: group, step: 'next2', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });

                                    ////console.logcard, '1');
                                }

                            } else {
                                // ////console.log'NO INFO FROM ID CARD')
                                ////console.logdatainfo, 'NEW PATIENT NO HN WITH INPUT FROM HN')
                                        console.log("CHECK20");
                                res.status(200).send({ success: true, idcard: '', worklistgroup: '', step: 'next1', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location, api_nocard: 'api_nocard' });

                                // ////console.logcard, '3');
                            }

                        });
                    }
                }
            });

        } else {
            console.log("CHECK21");
            res.status(200).send({ success: true, idcard: '', worklistgroup: '', step: 'next1', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location, api_nocard: 'api_nocard' });
        }
    } else if (ref) {
        ////console.log'Passport');
        chkRefNoFromDB({ refno: ref }, (res_chkrefno) => {
            //console.logres_chkrefno.data, "TEST CHK PATIENT FROM REFNO")
            if (res_chkrefno.success && res_chkrefno.data != '') {
                if (res_chkrefno.data.active == 'Y') {
                    //console.logres_chkrefno, "TEST REPRINT 5")
                    var group = '1';
                    getPatientRePrint({ uid: res_chkrefno.data.uid, group: res_chkrefno.data.worklistgroupuid }, (res_patientinfo) => {
                        //console.logres_patientinfo, "TEST REPRINT 5")                        
                        if (res_patientinfo.success && res_patientinfo.data != '') {
                            var datainfo = '';
                            clearKioskLocation(req.body.kiosk_location);
                            for (var i = 0; i < res_patientinfo.data.length; i++) {
                                
                                var flownameRoom = '';
                                // dataResult = res_patientinfo.data[i];
                                ////console.logres_patientinfo.data[i], "TEST REPRINT 7777777777")
                                var vscard1 = res_patientinfo.data[i].idcard.substr(0, 4, res_patientinfo.data[i].idcard);
                                var vscard2 = res_patientinfo.data[i].idcard.substr(4, 5, res_patientinfo.data[i].idcard);
                                var vscard3 = res_patientinfo.data[i].idcard.substr(9, 4, res_patientinfo.data[i].idcard);

                                var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                                var citizenid_xxx = vscard1 + (res_patientinfo.data[i].idcard == ''?'':vscard4) + vscard3;
                                // //console.logres_patientinfo.data[i].birthdate, "BIRTHDATE");
                                
                                if(res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != ''){
                                    flownameRoom = ' '+res_patientinfo.data[i].clinicname.replace(".",",");
                                }
                                datainfo = { 
                                    hnno: typeof hn === 'undefined'?'':hn, 
                                    idcard: res_patientinfo.data[i].idcard, 
                                    citizenid_xxx: typeof hn === 'citizenid_xxx'?'':citizenid_xxx, 
                                    prefixname_th: res_patientinfo.data[i].prename, 
                                    firstname_th: res_patientinfo.data[i].forename, 
                                    lastname_th: res_patientinfo.data[i].surname, 
                                    issuedate: res_patientinfo.data[i].birthdate, 
                                    patient_uid: res_patientinfo.data[i].patientuid 
                                };
                                
                                var dddd = {
                                    "location": '1',
                                    "sequence": i + 1,
                                    "title": res_patientinfo.data[i].title,
                                    "flowname": res_patientinfo.data[i].flowname+flownameRoom,
                                    "waiting": "",
                                    "queueno": res_patientinfo.data[i].queueno,
                                    "worklistuid": res_patientinfo.data[i].worklistuid,
                                    "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                                    "infoData": datainfo,
                                    "clinicname": res_patientinfo.data[i].clinicname,
                                    "group": res_patientinfo.data[i].worklistgroup,
                                    "closeselectone": res_patientinfo.data[i].closeselectone,
                                    "closevs": res_patientinfo.data[i].closevs,
                                    "closenewhn": res_patientinfo.data[i].closenewhn,
                                    "closepayor": res_patientinfo.data[i].closepayor,
                                    "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                                    "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                                    "cancelregister": res_patientinfo.data[i].cancelregister,
                                    "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                                    "kiosk_location": req.body.kiosk_location
                                }
                                global.endkiosk.push(dddd);
                                if (res_patientinfo.data.length == 1 || i == res_patientinfo.data.length - 1) {
                                    console.log("CHECK22");
                                    res.status(200).send({ success: true, data: global.endkiosk, step: 'next8', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });

                                }
                            }


                        }

                    });
                } else if(res_chkrefno.data.active == 'N'){
                    console.log("CHECK23");
                    res.status(200).send({ success: true, api_nocard: true});
                }else {
                    // var group = '1';
                    var dataResult = [];
                    getAppointmentDataAPI({ hnnumber: fullHn, dateappointment: datetime }, (res_getappointment) => {
                        if(res_getappointment.error == 500 || res_getappointment.error == 404){
                            console.log("CHECK24");
                            res.status(200).send({ success: false, error:500, api_his:true, api_appointment:false, scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                        }
                        if (res_getappointment.success && res_getappointment.data != '') {
                            var group = '2';
                            ////console.logres_getappointment, "DATA FROM API APPOINTMENT")
                            var vscard1 = idcard.substr(0, 4, idcard);
                            var vscard2 = idcard.substr(4, 5, idcard);
                            var vscard3 = idcard.substr(9, 4, idcard);

                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                            var citizenid_xxx = vscard1 + vscard4 + vscard3;
                            var datainfo = { hnno: hn, idcard: idcard, citizenid_xxx: citizenid_xxx, prefixname_th: 'คุณ', firstname_th: firstName, lastname_th: lastName, issuedate: birthDate, payor: '', type: '', payorname: '', payorcode: '' };
                            dataResult.push(datainfo);
                            var data = { location: '1', data: dataResult, dataAppointment: res_getappointment, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                            global.kiosk.push(data);
                            global.worklistGroup.push(group);
                            if(res_getappointment.error != 500 && res_getappointment.error != 404){
                                console.log("CHECK25");
                                res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                            }
                        } else {
                            var group = '1';
                            var vscard1 = res_chkhnno.data.idcard.substr(0, 4, res_chkhnno.data.idcard);
                            var vscard2 = res_chkhnno.data.idcard.substr(4, 5, res_chkhnno.data.idcard);
                            var vscard3 = res_chkhnno.data.idcard.substr(9, 4, res_chkhnno.data.idcard);

                            var vscard4 = vscard2.replace(vscard2, 'XXXXX');
                            var citizenid_xxx = vscard1 + vscard4 + vscard3;
                            var datainfo = { hnno: hn, idcard: res_chkhnno.data.idcard, citizenid_xxx: citizenid_xxx, prefixname_th: '', firstname_th: res_chkhnno.data.forename, lastname_th: res_chkhnno.data.surname, issuedate: res_chkhnno.data.dob, payor: res_chkhnno.data.payorid, type: res_chkhnno.data.pattienttypeid, payorname: '', payorcode: '' };
                            dataResult.push(datainfo);
                            var data = { location: '1', data: dataResult, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location };
                            global.kiosk.push(data);
                            global.worklistGroup.push(group);
                            if(res_getappointment.error != 500 && res_getappointment.error != 404){
                                console.log("CHECK26");
                                res.status(200).send({ success: true, idcard: card, citizenid_xxx: citizenid_xxx, worklistgroup: group, step: 'next6', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location });
                            }
                            ////console.logcard, '1');
                        }
                    });

                }

            } else {
                console.log("CHECK27");
                res.status(200).send({ success: true, idcard: '', worklistgroup: '', step: 'next1', scan: false, from_lc: '1', kiosk_location: req.body.kiosk_location, api_no_refno: 'api_no_refno' });
            }

        });
    }
});

var getPatientRePrint = (param, callback) => {
    ////console.logparam, "PARAMETER 1")
    console.log("param.uid " + param.uid);
    console.log("param.group " + param.group);
    var sql = "";
    // sql = "select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,max(queueno) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title from tr_patient c LEFT JOIN tr_patientqueue a ON c.uid = a.patientuid left join vw_worklistprocess b on(b.worklistgroup = '" + param.group + "' and(b.groupprocessuid = a.groupprocessuid or b.groupprocessuid is null)) where c.uid = '" + param.uid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle Order by worklistuid"
    sql = "select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,(select queueno from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and tr_patientqueue.groupprocessuid = max(b.groupprocessuid) limit 1) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title from vw_worklistprocess b LEFT outer JOIN tr_patient c on b.worklistgroup = '" + param.group + "' left outer join tr_patientqueue a ON c.uid = a.patientuid where c.uid = '" + param.uid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle,a.groupprocessuid Order by worklistuid";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                callback({ success: true, data: results.rows })
            } else {
                callback({ success: true, data: '' })
            }
        }
    });
}

var chkIdcardFromDB = (data, callback) => {
    //console.logdata.idcard, "CHK ID CARD PARAMETER")
    var sql = "SELECT * FROM tr_patient WHERE idcard = '" + data.idcard + "' AND cwhen::date = now()::date ORDER BY uid DESC LIMIT 1";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                ////console.logresults.rows[0], "RESULT 0 PARAMETER")
                callback({ success: true, data: results.rows[0] })
            } else {
                callback({ success: false })
            }
        }
    });
}

var chkHnNoFromDB = (data, callback) => {
    ////console.logdata)
    //console.logdata.hn, "CHK HN NO PARAMETER")

    var sql = "SELECT * FROM tr_patient WHERE hn = '" + data.hn + "' AND cwhen::date = now()::date ORDER BY uid DESC LIMIT 1";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                // //console.logresults.rows, "HN FROM DATABASE")
                callback({ success: true, data: results.rows[0] })
            } else {
                callback({ success: true, data: '' })
            }
        }
    });
}

var chkRefNoFromDB = (data, callback) => {
    ////console.logdata)
    //console.logdata.refno, "CHK REF NO PARAMETER")

    var sql = "SELECT * FROM tr_patient WHERE refno = '" + data.refno + "' AND cwhen::date = now()::date ORDER BY uid DESC LIMIT 1";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                // //console.logresults.rows, "HN FROM DATABASE")
                callback({ success: true, data: results.rows[0] })
            } else {
                callback({ success: true, data: '' })
            }
        }
    });
}

var chkApiOldPatient = (param, callback) => {
    //console.log(param, "CHK HN OLD 5321321")
    var test = 'http://191.123.95.31:8181/ords/pmkords/hlab/patient/' + param.hn + '/' + param.idcard;
    //console.logtest)
    axios({
        method: 'get',
        //url: 'http://127.0.0.1:9000/ords/pmkords/hlab/patient/' + param.hn + '/' + param.idcard,
        url: 'http://27.254.59.21:4001/ords/pmkords/hlab/patient/' + param.hn + '/' + param.idcard,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Basic YXJtOjEyMzQ="
        },
        timeout: 3000,
    }).then(function (response) {
        // console.log(response.data)
        // callback(aaa);
        if (response.data) {
            if(typeof response.data.data !== 'undefined'){
                response.data = response.data.data;
            }
            callback({ success: true, data: response.data })
        } else {
            callback({ success: true, data: 'ไม่พบ HN หรือบัตรประชาชนในระบบ' });
        }

    }).catch(error => {
        // console.log(error, "ERROR API FROM OLD PATIENT WITH INPUT")
        if(error.response){
            if (error.response.status == 404) {
                callback({ success: true, data: '' });
            } else if (error.response.status == 500) {
                callback({ success: true, error: 500 });
            }
        }else{
            callback({ success: true, error: 500 });
        }
    });
}

var getAppointmentDataAPI = (param, callback) => {
    ////console.logparam, 'parameter appointment')
    //https://pmk.express-apps.com/regissocket    
    //var productionurl = `https://pmk.express-apps.com/regissocket/ords/pmkords/hlab/appointments/${param.dateappointment}/null/${param.hnnumber}`;

    var productionurl = `http://27.254.59.21:4001/ords/pmkords/hlab/appointments/${param.dateappointment}/null/${param.hnnumber}`;
    axios({
        method: 'get',
        url: productionurl,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Basic YXJtOjEyMzQ="
        },
        timeout: 3000,
    }).then(function (response) {
        console.log("SUCCESS");
        // console.log(response.data, "APPOINTMENT")
        if (response.data) {
            if(typeof response.data.data !== 'undefined'){
                response.data = response.data.data;
            }
            callback({ success: true, data: response.data });
        } else {
            callback({ success: true, data: '' });
        }
    }).catch(error => {
        // console.log(error, "ERROR API FROM APPOINTMENT WITH INPUT")
        if(error.response == undefined){
            // console.log('5')
            callback({ success: true, error: 500 });
        }else{
            if(error.response){
                if (error.response.status == 404) {
                    callback({ success: true, data: '', error: 404 });
                } else if (error.response.status == 500) {
                    callback({ success: true, error: 500 });
                }
            }else{
                callback({ success: true, error: 500 });
            }
        }
        
    });
}

var checkStr = (val) => {
    var str = '<>&'; //ตัวอักษรที่ไม่ต้องการให้มี
    if (val.indexOf("/") != -1) return true //เครื่องหมาย '
    for (i = 0; i < str.length; i++) {
        if (val.indexOf(str.charAt(i)) != -1) return true
    }
    return false
}

// HN
router.post('/hn', (req, res) => {
    if (req.body.location == '1' && req.body.step == 'next3') {
        // console.log(req.body, "TEST API ERROR APPOINTMENT")
        res.status(200).send({ success: true, kiosk_location: req.body.kiosk_location });
        global.io.emit('kiosk_hn', global.kiosk[findIndex(1, req.body.kiosk_location)]);

        clearKioskNoHnLocation(req.body.kiosk_location);
    } else if (req.body.location == '1' && req.body.step == 'next2') {
        // HAVE HN
        res.status(200).send({ success: true, message: 'No HN', kiosk_location: req.body.kiosk_location });
        global.io.emit('kiosk_hn', global.kiosk[findIndex(1, req.body.kiosk_location)]);

        clearKioskNoHnLocation(req.body.kiosk_location);
    } else if (req.body.location == '1' && req.body.step == 'next8') {
        //console.log(global.endkiosk, 'test kiosk hn 3');
        const arr2 = global.endkiosk.filter((d) => {
            return d.kiosk_location == req.body.kiosk_location;
        });
        res.status(200).send({ success: true, kiosk_location: req.body.kiosk_location, data: arr2 });
        global.io.emit('kiosk_hn', arr2);
        clearKioskLocation(req.body.kiosk_location);
    }
});
// End HN

var clearKioskLocation = (param) => {
    const arr1 = global.endkiosk.filter((d) => {
        return d.kiosk_location != param;
    });
    global.endkiosk = arr1;
}

var clearKioskNoHnLocation = (param) => {
    const arr1 = global.kiosk.filter((d) => {
        return d.kiosk_location != param;
    });
    global.kiosk = arr1;
}
module.exports = router;