const router = require('express').Router();
const { Pool } = require('pg');
const config = require('./../function/connect');
const findIndexSlip = require('../function/findIndexSlip');
const deleteIndexAll = require('./../function/deleteIndexAll');

var qs = require('qs');

const axios = require('axios');
const pool = new Pool(config);

router.post('/reprintslip', (req, res) => {
    // res.status(200).json({success:true})
    if (req.body.reprint == '1' && req.body.location == '8' && req.body.step == 'next8') {
        //console.logreq.body, "REPRINTSLIP!!!!")
        var patientuid = '';
        var idcard = '';
        var hn = '-';
        var data = '';
        var wlgroup = '';
        //console.log(req.body, "REPRINT !")

        if (req.body.patientuid && req.body.idcard) {
            patientuid = req.body.patientuid;
            idcard = req.body.idcard;
            if (req.body.hnno) {
                hn = req.body.hnno;
            }
            wlgroup = req.body.group;
            var listgroupname = '';

            getPatientRePrint({ patientuid: patientuid, group: wlgroup }, (res_patientinfo) => {
                //console.logres_patientinfo, "TEST REPRINT")
                if (res_patientinfo) {
                    var datainfo = '';
                    var aaa = [];
                    clearKioskLocation(req.body.kiosk_location);

                    if (wlgroup == '1') {
                        listgroupname = 'Walk-in';
                    } else if (wlgroup == '2') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '3') {
                        listgroupname = 'ผู้ป่วยใหม่';
                    } else if (wlgroup == '4') {
                        listgroupname = 'Walk-in';
                    } else if (wlgroup == '5') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '6') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '7') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '8') {
                        listgroupname = 'Walk-in';
                    } else if (wlgroup == '9') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '10') {
                        listgroupname = 'นัดหมาย';
                    }
                    for (var i = 0; i < res_patientinfo.data.length; i++) {
                        var flownameRoom = '';
                        // dataResult = res_patientinfo.data[i];
                        // //console.logdataResult, "TEST REPRINT")
                        if (res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != '') {
                            flownameRoom = ' ' + res_patientinfo.data[i].clinicname.replace(".", ",");
                        }
                        //console.log(flownameRoom, "TEST CLINIC NAME NO QUEUENUMBER");
                        datainfo = { 
                            hnno: hn, 
                            idcard: res_patientinfo.data[i].idcard, 
                            prefixname_th: res_patientinfo.data[i].prename, 
                            firstname_th: res_patientinfo.data[i].forename, 
                            lastname_th: res_patientinfo.data[i].surname, 
                            issuedate: res_patientinfo.data[i].dob, 
                            patient_uid: res_patientinfo.data[i].patientuid, 
                            patient_cwhen: res_patientinfo.data[i].patient_cwhen, 
                            worklistgroup: res_patientinfo.data[i].worklistgroup, 
                            refnoxx: res_patientinfo.data[i].refno 
                        };
                        var dddd = {
                            "location": '8',
                            "sequence": i + 1,
                            "title": res_patientinfo.data[i].title,
                            "flowname": res_patientinfo.data[i].flowname + flownameRoom,
                            "waitinghn": res_patientinfo.data[i].count_patient_newhn,
                            "waitingpayor": res_patientinfo.data[i].count_patient_payor,
                            "queueno": res_patientinfo.data[i].queueno,
                            "worklistuid": res_patientinfo.data[i].worklistuid,
                            "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                            "infoData": datainfo,
                            "group": res_patientinfo.data[i].worklistgroup,
                            "closeselectone": res_patientinfo.data[i].closeselectone,
                            "closevs": res_patientinfo.data[i].closevs,
                            "closenewhn": res_patientinfo.data[i].closenewhn,
                            "closepayor": res_patientinfo.data[i].closepayor,
                            "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                            "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                            "cancelregister": res_patientinfo.data[i].cancelregister,
                            "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                            "patientcometype": listgroupname,
                            "allcounter": res_patientinfo.data[i].allcounter,
                            "kiosk_location": req.body.kiosk_location
                        }
                        aaa.push(dddd);
                        global.endkiosk.push(dddd);
                        if (i == res_patientinfo.data.length - 1) {
                            res.status(200).send({ success: true, data: global.endkiosk, step: 'next7', scan: false, from_lc: '8', kiosk_location: req.body.kiosk_location });

                        }
                    }


                }

            });

        }else if (req.body.patientuid) {
            patientuid = req.body.patientuid;
            if (req.body.hnno) {
                hn = req.body.hnno;
            }
            wlgroup = req.body.group;
            var listgroupname = '';

            getPatientRePrint({ patientuid: patientuid, group: wlgroup }, (res_patientinfo) => {
                //console.logres_patientinfo, "TEST REPRINT")
                if (res_patientinfo) {
                    var datainfo = '';
                    var aaa = [];
                    clearKioskLocation(req.body.kiosk_location);

                    if (wlgroup == '1') {
                        listgroupname = 'Walk-in';
                    } else if (wlgroup == '2') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '3') {
                        listgroupname = 'ผู้ป่วยใหม่';
                    } else if (wlgroup == '4') {
                        listgroupname = 'Walk-in';
                    } else if (wlgroup == '5') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '6') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '7') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '8') {
                        listgroupname = 'Walk-in';
                    } else if (wlgroup == '9') {
                        listgroupname = 'นัดหมาย';
                    } else if (wlgroup == '10') {
                        listgroupname = 'นัดหมาย';
                    }
                    for (var i = 0; i < res_patientinfo.data.length; i++) {
                        var flownameRoom = '';
                        // dataResult = res_patientinfo.data[i];
                        // //console.logdataResult, "TEST REPRINT")
                        if (res_patientinfo.data[i].worklistuid == 6 && res_patientinfo.data[i].clinicname != '') {
                            flownameRoom = ' ' + res_patientinfo.data[i].clinicname.replace(".", ",");
                        }
                        //console.log(flownameRoom, "TEST CLINIC NAME NO QUEUENUMBER");
                        datainfo = { 
                            hnno: typeof hn == 'undefined'?'':hn, 
                            idcard: typeof res_patientinfo.data[i].idcard == 'undefined'?'':res_patientinfo.data[i].idcard, 
                            prefixname_th: res_patientinfo.data[i].prename, 
                            firstname_th: res_patientinfo.data[i].forename, 
                            lastname_th: res_patientinfo.data[i].surname, 
                            issuedate: res_patientinfo.data[i].dob, 
                            patient_uid: res_patientinfo.data[i].patientuid, 
                            patient_cwhen: res_patientinfo.data[i].patient_cwhen, 
                            worklistgroup: res_patientinfo.data[i].worklistgroup, 
                            refnoxx: res_patientinfo.data[i].refno 
                        };
                        var dddd = {
                            "location": '8',
                            "sequence": i + 1,
                            "title": res_patientinfo.data[i].title,
                            "flowname": res_patientinfo.data[i].flowname + flownameRoom,
                            "waitinghn": res_patientinfo.data[i].count_patient_newhn,
                            "waitingpayor": res_patientinfo.data[i].count_patient_payor,
                            "queueno": res_patientinfo.data[i].queueno,
                            "worklistuid": res_patientinfo.data[i].worklistuid,
                            "groupprocessuid": res_patientinfo.data[i].groupprocessuid,
                            "infoData": datainfo,
                            "group": res_patientinfo.data[i].worklistgroup,
                            "closeselectone": res_patientinfo.data[i].closeselectone,
                            "closevs": res_patientinfo.data[i].closevs,
                            "closenewhn": res_patientinfo.data[i].closenewhn,
                            "closepayor": res_patientinfo.data[i].closepayor,
                            "count_patient_newhn": res_patientinfo.data[i].count_patient_newhn,
                            "count_patient_payor": res_patientinfo.data[i].count_patient_payor,
                            "cancelregister": res_patientinfo.data[i].cancelregister,
                            "cancelnewhn": res_patientinfo.data[i].cancelnewhn,
                            "patientcometype": listgroupname,
                            "allcounter": res_patientinfo.data[i].allcounter,
                            "kiosk_location": req.body.kiosk_location
                        }
                        aaa.push(dddd);
                        global.endkiosk.push(dddd);
                        if (i == res_patientinfo.data.length - 1) {
                            res.status(200).send({ success: true, data: global.endkiosk, step: 'next7', scan: false, from_lc: '8', kiosk_location: req.body.kiosk_location });

                        }
                    }


                }

            });

        }
    }

});

router.post('/discard_queue', (req, res) => {
    if (req.body.DISCARD == '1') {
        var patientuid = req.body.patient_uid;
        //console.log(req.body, "DISCARD")
        var alldata = JSON.parse(req.body.alldata_session);
        // console.log(alldata, "ALL DATA USER INFO")
        global.io.emit('remove_queue_puid', { data: patientuid });
        if (req.body.alldata_group == '1' || req.body.alldata_group == '2' || req.body.alldata_group == '4' || req.body.alldata_group == '5' || req.body.alldata_group == '6' || req.body.alldata_group == '7' || req.body.alldata_group == '8' || req.body.alldata_group == '9' || req.body.alldata_group == '10') {
            res.send({ success: true, step: 'next6', discard: '1', kiosk_location: req.body.kiosk_location });
        } else if (req.body.alldata_group == '3') {
            res.send({ success: true, step: 'next2', discard: '1', kiosk_location: req.body.kiosk_location });
        }
    }
});

var getPatientRePrint = (param, callback) => {
    // var sql = "select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,max(queueno) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title from tr_patient c LEFT JOIN tr_patientqueue a ON c.uid = a.patientuid left join vw_worklistprocess b on(b.worklistgroup = '" + param.group + "' and(b.groupprocessuid = a.groupprocessuid or b.groupprocessuid is null)) where c.uid = '" + param.patientuid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle Order by worklistuid"
    var sql = "select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,(select queueno from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and tr_patientqueue.groupprocessuid = max(b.groupprocessuid) limit 1) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title from vw_worklistprocess b LEFT outer JOIN tr_patient c on b.worklistgroup = '" + param.group + "' left outer join tr_patientqueue a ON c.uid = a.patientuid where c.uid = '" + param.patientuid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle,a.groupprocessuid Order by worklistuid";
    pool.query(sql, (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                //console.logresults.rows, "RESULT RESULT WORKLIST PROCESS")
                callback({ success: true, data: results.rows })
            } else {
                callback({ success: false })
            }
        }
    });
}

var clearKioskLocation = (param) => {
    const arr1 = global.endkiosk.filter((d) => {
        return d.kiosk_location != param;
    });
    global.endkiosk = arr1;
}


module.exports = router;