const router = require('express').Router();
const { Pool } = require('pg');
const config = require('./../function/connect');
var dateFormat = require('dateformat');

const axios = require('axios');
const pool = new Pool(config);

var d = new Date();
var now = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
var datetime = dateFormat(now, "yyyy-mm-dd");


router.post('/openconverage', (req, res) => {
    if(req.body.hn && req.body.idcard){
        openConverage({idcard:req.body.idcard,hn:req.body.hn,payorcode:req.body.payorcode,payorname:req.body.payorname}, (res_postconverage) => {
            if(res_postconverage.success){
                res.send({status: true});
            }else{
                res.send({status: false});
            }
        })
    }
})

var openConverage = (param, callback) => {
    axios({
        method: 'post',
        url: "http://191.123.58.33:5001/dev/opencoverage",
        headers: {
            "Content-Type": "application/json"
        },
        data: {
            pid: param.idcard,
            hn: param.hn,
            payor: param.payorcode,
            policyHolder: param.payorname,
        }

    }).then(function (response) {
        if (response.data) {
            callback({ success: true });
        }else{
            callback({ success: false });
        }
    });
}

// var openConverage = (param, callback) => {
//     axios({
//         method: 'post',
//         url: "http://191.123.58.33:5001/dev/opencoverage",
//         headers: {
//             "Content-Type": "application/json"
//         },
//         data: {
//             pid: param.idcard,
//             hn: param.hn,
//             payor: param.payorcode,
//             policyHolder: param.payorname,
//         }

//     }).then(function (response) {
//         if (response.data) {
//             callback({ success: true });
//         }else{
//             callback({ success: false });
//         }
//     });
// }











module.exports = router;