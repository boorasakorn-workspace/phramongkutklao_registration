const router = require('express').Router();
const { Pool } = require('pg');
const jwt = require('jsonwebtoken');
const axios = require('axios');
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test_jwt',
    password: '23241167',
    port: 5432
});

let startGet = getData();

// ------------------------ JWT Authen & Check token expire
async function getData(req, res, next){
    // let getPayload = await getJwt1();
    // await login('datajwt',getPayload);
    let chkToken = await selectData();
    // //console.logchkToken.token);
    
        var decodeToken = decoded(chkToken.token);
        // //console.logdecodeToken);
        var expire = decodeToken.expire;
        var iat = decodeToken.iat;
        var exp = decodeToken.exp;
        // //console.logexpire);
        if(Date.now() >= exp  * 1000){
            let payload = await login();
            // //console.logpayload.data);
            var token = payload.data.token;
            var decodeAuth = decoded(payload.data.token);
            //console.logdecodeAuth, 'authentoken');
            await insertToken(decodeAuth, token);
            //console.log'1');
        }else{
            // //console.logchkToken.token);
            //console.log'2');
            let getResource = await tokenGetData(chkToken.token);
            // //console.loggetResource, 'Get DATA');
            //console.loggetResource.data);
        }


    // await chkToken(chkToken);
    // await tokenGetData();
    // return payload.data.token;
}

function decoded(token){
    return jwt.decode(token);
}

async function selectData(){
    return new Promise((resolve, reject) => {
        pool.connect(function(err) {
            if (err) throw err ;
                pool.query("SELECT * FROM login_user ORDER BY id DESC LIMIT 1", function (err, result) {
                    if (!err){
                        //Return the fields object:
                        if(result){
                            // //console.logresult);
                            resolve(result.rows[0]);
                        }else{
                            //console.log'No Data');
                        }
                    }
                });
        });
    });
}

async function insertToken(data, token){
    // //console.logdata, 'insert');
    // //console.logtoken, 'token');
    return new Promise((resolve, reject) => {
        pool.connect(function(err) {
              pool.query("INSERT INTO login_user(token,iat,exp) VALUES ('"+token+"','"+data.iat+"','"+data.exp+"')", function (err, results) {
                    if (!err){
                        //Return the fields object:
                        if(results){
                            //   //console.log'OK');
                            resolve('200');
                        }
                    }else{
                        //console.log'ERROR');
                    }
              });
        });
    });
}

async function login(){
    var result = await axios({
      method: 'get',
      url: 'http://192.168.0.97:3000/signin',
      headers: { 
        'Authorization' : 'Bearer ZDJhYmFhMzdhN2MzZGIxMTM3ZDM4NWUxZDhjMTVmZDI=',
        'Content-Type': 'application/json'
      }
    }).then(function(response){
        return response;
        // //console.logresponse);
    }).catch (error => {
        //console.log'Cannot Get URL');
    });
    return result;
}

async function tokenGetData(token){
    var result = await axios({
      method: 'get',
      url: 'http://192.168.0.97:3000/getdata',
      headers: { 
        'Authorization' : 'Bearer '+token,
        'Content-Type': 'application/json'
      }
    }).then(function(response){
        return response;
        // //console.logresponse);
    }).catch (error => {
        //console.log'Cannot Get URL');
    });
    return result;
    // //console.logresult);
}
// ------------------------ End JWT Authen & Check token expire

module.exports = router;