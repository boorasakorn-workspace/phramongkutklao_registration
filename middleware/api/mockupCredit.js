const router = require('express').Router();
var dateFormat = require('dateformat');

const axios = require('axios');
const fs = require('fs');

router.get('/credit/:year/:hn/:ignore?/:datetime?', async (req, res) => {
    //credit with get
    //ex. http://27.254.59.21/ords/pmkords/hlab/credit/${fullHn}/null/${param.datetime}
    if (req.params.year && req.params.hn) {
        //get hn&&year
        let fullHn = `${req.params.hn}/${req.params.year}`;
        let lastPayor = await getLastPayorByHN({hn:fullHn}); //mockup use hn/year format
        await res.status(200).send(lastPayor);
    }
});

const getLastPayorByHN = async (param) => {
    if(param.hn){
        const readFile = await fs.readFileSync('./mockupjson/onsite-pmk/api-currentcredit.json', 'utf8');
        if (readFile) {
            readJSON = await JSON.parse(readFile.toString());
            const arr2 = await readJSON.filter((d) => {
                return d.hn == param.hn;
            });
            if(arr2.length){
                return ({success: true, data: arr2[arr2.length - 1],data1: arr2})
            }else if(arr2.length == 1){
                return ({success: true, data: arr2, data1: arr2})
            }else{
                return ({success: true, data: '', data1: ''})
            }
        }else{
            return ({success: true, data: ''})
        }
    }
}

module.exports = router;

const ifyouwannasuicidethisisyourchancereadthisyear = () => {
    //console.log(req.body, "PARAMETER POST CREDIT")
    if (req.body.p_run_hn) {
        axios({
            method: 'post',
            url: 'http://27.254.59.21/ords/pmkords/hlab/credit/',
            headers: {
                "Content-Type": "application/json"
            },
            data:req.body,

        }).then(response => {
            var hnnum = req.body.p_run_hn + '/' + req.body.p_year_hn;
            //console.log(response, "OPEN CREDIT ACTIVE")
            var aaa = [];
                const arr2 = aaa.filter((d) => {
                    return d.hn == hnnum && d.policyHolderId == req.body.p_credit_id;
                });
            // callback(aaa);
            if (response.data) {
                res.status(200).send({success: true, data: response.data.data })
            } else {
                res.status(200).send({success: true, data: '' });
            }

        }).catch(error => {
            // //console.log(error.response.status, "ERROR CREDIT INACTIVE")
            if (error.response.status == 404) {
                res.status(200).send({success: false, error: 404 });
            } else if (error.response.status == 403) {
                res.status(200).send({success: false, error: 403 });
            } else if (error.response.status == 500) {
                res.status(200).send({success: false, error: 500 });
            }
        });
    }
}