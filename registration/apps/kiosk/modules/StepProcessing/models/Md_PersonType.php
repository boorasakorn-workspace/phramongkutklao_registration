<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Md_PersonType extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->libraries('database');
    }

    public function update_queue()
    {

        $patient_uid = $this->input->post('patient_uid');
        $active = $this->input->post('active');

        $set = array(
            'active' => $active
        );

        $this->db->where('uid', $patient_uid)
            ->update('tr_patient', $set);
        
        $datapcc = array(
            'patientuid' => $patient_uid,
            'worklistuid' => '17',
            'createdate'=>date('Y-m-d H:i:s')
        );

        $this->db->insert('tr_processcontrol',$datapcc);    

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function getworklist()
    {
        $patient_uid = $this->input->post('patient_uid');

        $this->db->select('*')
            ->from('vw_patientprocess')
            ->where('patientuid',$patient_uid)
            ->order_by('worklistuid','asc');
       
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    public function getpersontype(){
        
        $this->db->select('*')
        ->from('tb_patienttype')
        ->order_by('order','ASC');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getpayor(){
        
        $this->db->select('*')
        ->from('tb_payor')
        ->order_by('order','ASC');
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

}
